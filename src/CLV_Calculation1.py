import csv
import os

import datetime

from enum import Enum

import myLog
from redshift import query_redshift


class Status(Enum):
    zero_profits = 1
    new_customer = 2
    old_customer = 3
    new_churn = 4
    old_churn = 5
    came_back = 6


class Customer:
    def __init__(self, row):
        self.customer_id, self.sum_profits, self.sum_profits1, self.sum_profits2, self.sum_profits3, \
            self.sum_profits4, self.sum_profits5, self.sum_profits6, self.sum_profit_future = row
        self.status = get_status(self.sum_profits1, self.sum_profits2, self.sum_profits3,
                                 self.sum_profits4, self.sum_profits5, self.sum_profits6)
        self.months_active = get_months_active_numbers(self.sum_profits1, self.sum_profits2, self.sum_profits3,
                                                       self.sum_profits4, self.sum_profits5, self.sum_profits6)


src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr) - 1])
output_directory = os.path.join(project_directory, "output")
clv_output_directory = os.path.join(output_directory, "CLV")

sql_script = """
            SELECT t1.customer_id,
                   sum_profit,
                   sum_profit1,
                   sum_profit2,
                   sum_profit3,
                   sum_profit4,
                   sum_profit5,
                   sum_profit6,
                   sum_profit_future
            FROM (
            SELECT customer_id,
                  SUM(profitusd) sum_profit,
                  SUM(profitusd*(CASE WHEN close_time > dateadd(day, -180, a_date) AND close_time < dateadd(day, -150, a_date) AND close_time <> '1970-01-01' THEN 1.0 ELSE 0.0 END)) sum_profit1,
                  SUM(profitusd*(CASE WHEN close_time > dateadd(day, -150, a_date) AND close_time < dateadd(day, -120, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) sum_profit2,
                  SUM(profitusd*(CASE WHEN close_time > dateadd(day, -120, a_date) AND close_time < dateadd(day, -90, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) sum_profit3,
                  SUM(profitusd*(CASE WHEN close_time > dateadd(day, -90, a_date) AND close_time < dateadd(day, -60, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) sum_profit4,
                  SUM(profitusd*(CASE WHEN close_time > dateadd(day, -60, a_date) AND close_time < dateadd(day, -30, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) sum_profit5,
                  SUM(profitusd*(CASE WHEN close_time > dateadd(day, -30, a_date) AND close_time < a_date AND close_time <> '1970-01-01' THEN 1.0 ELSE 0.0 END)) sum_profit6

            FROM y_offline_positions_from_cosmos
            JOIN tmp_vars ON close_time <> '1970-01-01' AND close_time < a_date  AND close_time > dateadd(day, -180, a_date)
            GROUP BY customer_id
            ) t1
            JOIN (SELECT customer_id, SUM(profitusd) sum_profit_future FROM y_offline_positions_from_cosmos GROUP BY customer_id) t2 ON t1.customer_id = t2.customer_id
            ;
             """


def get_status(sum_profits1, sum_profits2, sum_profits3, sum_profits4, sum_profits5, sum_profits6):
    if sum_profits1 == 0 and sum_profits2 == 0 and sum_profits3 == 0 and sum_profits4 == 0 and \
       sum_profits5 == 0:
        if sum_profits6 == 0:
            return Status.zero_profits
        else:
            return Status.new_customer
    elif sum_profits5 != 0:
        if sum_profits6 != 0:
            return Status.old_customer
        else:
            return Status.new_churn
    elif sum_profits5 == 0 and sum_profits6 == 0:
        return Status.old_churn
    elif sum_profits6 != 0 and (sum_profits1 != 0 or sum_profits2 != 0 or
                                sum_profits3 != 0 or sum_profits4 != 0 or
                                sum_profits5 != 0):
        return Status.came_back


def get_months_active_numbers(sum_profits1, sum_profits2, sum_profits3, sum_profits4, sum_profits5, sum_profits6):
    return sum(1 for x in [sum_profits1, sum_profits2, sum_profits3, sum_profits4, sum_profits5, sum_profits6]
               if x != 0.0)


def output_clv_one_group(customers, status, churn_rate_1, churn_rate_2, time_str):
    path_csv = os.path.join(clv_output_directory, "clv_" + status + "_" + time_str + ".csv")
    churn_rate_1_str = "months_avg_profit / " + str(churn_rate_1)
    churn_rate_2_str = "months_avg_profit / " + str(churn_rate_2)
    discount1 = 0
    discount2 = 0.1
    discount3 = 1.1
    retention_rate_1 = 1 - churn_rate_1
    retention_rate_2 = 1 - churn_rate_2
    retention_rate_1_str = "retention = " + str(retention_rate_1) + " discount = " + str(discount1)
    retention_rate_2_str = "retention = " + str(retention_rate_2) + " discount = " + str(discount1)
    retention_rate_3_str = "retention = " + str(retention_rate_1) + " discount = " + str(discount2)
    retention_rate_4_str = "retention = " + str(retention_rate_2) + " discount = " + str(discount2)
    retention_rate_5_str = "retention = " + str(retention_rate_1) + " discount = " + str(discount3) + "_2"
    retention_rate_6_str = "retention = " + str(retention_rate_2) + " discount = " + str(discount3) + "_2"
    retention_rate_7_str = "retention = " + str(retention_rate_1) + " discount = " + str(discount2) + "_2"
    retention_rate_8_str = "retention = " + str(retention_rate_2) + " discount = " + str(discount2) + "_2"

    # write output_analytics_list
    with open(path_csv, 'wb') as csv_file:
        fieldnames = ['customer_id', 'sum_profit', 'months_active', 'months_avg_profit',
                      churn_rate_1_str, churn_rate_2_str, retention_rate_1_str, retention_rate_2_str,
                      retention_rate_3_str, retention_rate_4_str, retention_rate_5_str, retention_rate_6_str,
                      retention_rate_7_str, retention_rate_8_str, 'sum_profit_future',
                      'diff_abs_' + churn_rate_1_str, 'diff_abs_' + churn_rate_2_str, 'diff_abs_' + retention_rate_1_str,
                      'diff_abs_' + retention_rate_2_str, 'diff_abs_' + retention_rate_3_str,
                      'diff_abs_' + retention_rate_4_str, 'diff_abs_' + retention_rate_5_str,
                      'diff_abs_' + retention_rate_6_str, 'diff_abs_' + retention_rate_7_str,
                      'diff_abs_' + retention_rate_8_str]

        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for customer in customers:
            if customer.months_active != 0:
                months_avg_profit = float(customer.sum_profits) / customer.months_active
            else:
                months_avg_profit = 0
            churn_rate_1_calc = months_avg_profit / churn_rate_1
            churn_rate_2_calc = months_avg_profit / churn_rate_2
            retention_rate_1_calc = float(customer.sum_profits*retention_rate_1)/(1+discount1-retention_rate_1)
            retention_rate_2_calc = float(customer.sum_profits*retention_rate_2)/(1+discount1-retention_rate_2)
            retention_rate_3_calc = float(customer.sum_profits*retention_rate_1)/(1+discount2-retention_rate_1)
            retention_rate_4_calc = float(customer.sum_profits*retention_rate_2)/(1+discount2-retention_rate_2)
            retention_rate_5_calc = float(customer.sum_profits*retention_rate_1)/(1+discount1)
            retention_rate_6_calc = float(customer.sum_profits*retention_rate_2)/(1+discount1)
            retention_rate_7_calc = float(customer.sum_profits*retention_rate_1)/(1+discount2)
            retention_rate_8_calc = float(customer.sum_profits*retention_rate_2)/(1+discount2)
            writer.writerow({'customer_id': str(customer.customer_id),
                             'sum_profit': str(customer.sum_profits),
                             'months_active': str(customer.months_active),
                             'months_avg_profit': str(months_avg_profit),
                             churn_rate_1_str: str(churn_rate_1_calc),
                             churn_rate_2_str: str(churn_rate_2_calc),
                             retention_rate_1_str: str(retention_rate_1_calc),
                             retention_rate_2_str: str(retention_rate_2_calc),
                             retention_rate_3_str: str(retention_rate_3_calc),
                             retention_rate_4_str: str(retention_rate_4_calc),
                             retention_rate_5_str: str(retention_rate_5_calc),
                             retention_rate_6_str: str(retention_rate_6_calc),
                             retention_rate_7_str: str(retention_rate_7_calc),
                             retention_rate_8_str: str(retention_rate_8_calc),
                             'sum_profit_future': str(customer.sum_profit_future),
                             'diff_abs_' + churn_rate_1_str: str(abs(customer.sum_profit_future - churn_rate_1_calc)),
                             'diff_abs_' + churn_rate_2_str: str(abs(customer.sum_profit_future - churn_rate_2_calc)),
                             'diff_abs_' + retention_rate_1_str: str(abs(customer.sum_profit_future - retention_rate_1_calc)),
                             'diff_abs_' + retention_rate_2_str: str(abs(customer.sum_profit_future - retention_rate_2_calc)),
                             'diff_abs_' + retention_rate_3_str: str(abs(customer.sum_profit_future - retention_rate_3_calc)),
                             'diff_abs_' + retention_rate_4_str: str(abs(customer.sum_profit_future - retention_rate_4_calc)),
                             'diff_abs_' + retention_rate_5_str: str(abs(customer.sum_profit_future - retention_rate_5_calc)),
                             'diff_abs_' + retention_rate_6_str: str(abs(customer.sum_profit_future - retention_rate_6_calc)),
                             'diff_abs_' + retention_rate_7_str: str(abs(customer.sum_profit_future - retention_rate_7_calc)),
                             'diff_abs_' + retention_rate_8_str: str(abs(customer.sum_profit_future - retention_rate_8_calc))
                             })
        csv_file.close()


def simple_clv(train_date):
    query_date_string = """-- reference date table
                    DROP TABLE IF EXISTS tmp_vars;
                    CREATE temp TABLE tmp_vars (
                      a_date DATE
                    );
                    INSERT INTO tmp_vars (a_date) VALUES
                      ('%(date)s')
                     ;\n""" % {"date": train_date}

    final_query = query_date_string + sql_script
    rows = query_redshift(final_query)
    customers_zero_profits = []
    customers_new_customers = []
    customers_old_customers = []
    customers_new_churn = []
    customers_old_churn = []
    customers_came_back = []
    for row in rows:
        customer = Customer(row)
        if customer.status == Status.zero_profits:
            customers_zero_profits.append(customer)
        elif customer.status == Status.new_customer:
            customers_new_customers.append(customer)
        elif customer.status == Status.old_customer:
            customers_old_customers.append(customer)
        elif customer.status == Status.new_churn:
            customers_new_churn.append(customer)
        elif customer.status == Status.old_churn:
            customers_old_churn.append(customer)
        elif customer.status == Status.came_back:
            customers_came_back.append(customer)

    myLog.log('customers_zero_profits = ' + str(len(customers_zero_profits)))
    myLog.log('customers_new_customers = ' + str(len(customers_new_customers)))
    myLog.log('customers_old_customers = ' + str(len(customers_old_customers)))
    myLog.log('customers_new_churn = ' + str(len(customers_new_churn)))
    myLog.log('customers_old_churn = ' + str(len(customers_old_churn)))
    myLog.log('customers_came_back = ' + str(len(customers_came_back)))

    churn_rate_1 = float(len(customers_new_churn)) / (len(customers_old_customers) + len(customers_new_churn))
    churn_rate_2 = float(len(customers_new_churn)) / (len(customers_old_customers) + len(customers_new_churn) + len(customers_came_back))
    myLog.log('customers_new_churn / (customers_old_customers + customers_new_churn) = ' + str(churn_rate_1))
    myLog.log('customers_new_churn / (customers_old_customers + customers_new_churn + customers_came_back) = ' + str(churn_rate_2))

    now = datetime.datetime.now()
    time_str = now.strftime("%Y%m%dT%H%M%S")

    output_clv_one_group(customers_zero_profits, "customers_zero_profits", churn_rate_1, churn_rate_2, time_str)
    output_clv_one_group(customers_new_customers, "customers_new_customers", churn_rate_1, churn_rate_2, time_str)
    output_clv_one_group(customers_old_customers, "customers_old_customers", churn_rate_1, churn_rate_2, time_str)
    output_clv_one_group(customers_new_churn, "customers_new_churn", churn_rate_1, churn_rate_2, time_str)
    output_clv_one_group(customers_old_churn, "customers_old_churn", churn_rate_1, churn_rate_2, time_str)
    output_clv_one_group(customers_came_back, "customers_came_back", churn_rate_1, churn_rate_2, time_str)


simple_clv('2015-08-01')
