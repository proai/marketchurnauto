import csv
import os

import datetime

from enum import Enum

import myLog
from redshift import query_redshift


class Status(Enum):
    new_customer = 1
    old_customer = 2
    new_churn = 3
    old_churn = 4
    came_back = 5


class Customer:
    def __init__(self, row):
        self.customer_id, self.count_cmd, self.count_cmd1, self.count_cmd2, self.count_cmd3, self.count_cmd4, \
            self.count_cmd5, self.count_cmd6, self.sum_amount, self.sum_amount1, self.sum_amount2, self.sum_amount3,\
            self.sum_amount4, self.sum_amount5, self.sum_amount6, self.count_cmd_future, self.sum_amount_future = row
        # self.status = get_status(self.count_cmd1, self.count_cmd2, self.count_cmd3,
        #                          self.count_cmd4, self.count_cmd5, self.count_cmd6)
        self.months_active = get_months_active_numbers(self.count_cmd1, self.count_cmd2, self.count_cmd3,
                                                       self.count_cmd4, self.count_cmd5, self.count_cmd6)

src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr) - 1])
output_directory = os.path.join(project_directory, "output")
clv_output_directory = os.path.join(output_directory, "CLV")

sql_script = """


SELECT t1.customer_id,
       count_cmd,
       count_cmd1,
       count_cmd2,
       count_cmd3,
       count_cmd4,
       count_cmd5,
       count_cmd6,
       sum_amount,
       sum_amount1,
       sum_amount2,
       sum_amount3,
       sum_amount4,
       sum_amount5,
       sum_amount6,
       count_cmd_future,
       sum_amount_future
FROM (
SELECT customer_id,
      SUM(amount) sum_amount,
      SUM(amount*(CASE WHEN close_time > dateadd(day, -180, a_date) AND close_time < dateadd(day, -150, a_date) AND close_time <> '1970-01-01' THEN 1.0 ELSE 0.0 END)) sum_amount1,
      SUM(amount*(CASE WHEN close_time > dateadd(day, -150, a_date) AND close_time < dateadd(day, -120, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) sum_amount2,
      SUM(amount*(CASE WHEN close_time > dateadd(day, -120, a_date) AND close_time < dateadd(day, -90, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) sum_amount3,
      SUM(amount*(CASE WHEN close_time > dateadd(day, -90, a_date) AND close_time < dateadd(day, -60, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) sum_amount4,
      SUM(amount*(CASE WHEN close_time > dateadd(day, -60, a_date) AND close_time < dateadd(day, -30, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) sum_amount5,
      SUM(amount*(CASE WHEN close_time > dateadd(day, -30, a_date) AND close_time < a_date AND close_time <> '1970-01-01' THEN 1.0 ELSE 0.0 END)) sum_amount6,
      COUNT(*) count_cmd,
      SUM((CASE WHEN close_time > dateadd(day, -180, a_date) AND close_time < dateadd(day, -150, a_date) AND close_time <> '1970-01-01' THEN 1.0 ELSE 0.0 END)) count_cmd1,
      SUM((CASE WHEN close_time > dateadd(day, -150, a_date) AND close_time < dateadd(day, -120, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) count_cmd2,
      SUM((CASE WHEN close_time > dateadd(day, -120, a_date) AND close_time < dateadd(day, -90, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) count_cmd3,
      SUM((CASE WHEN close_time > dateadd(day, -90, a_date) AND close_time < dateadd(day, -60, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) count_cmd4,
      SUM((CASE WHEN close_time > dateadd(day, -60, a_date) AND close_time < dateadd(day, -30, a_date) AND close_time <> '1970-01-01'  THEN 1.0 ELSE 0.0 END)) count_cmd5,
      SUM((CASE WHEN close_time > dateadd(day, -30, a_date) AND close_time < a_date AND close_time <> '1970-01-01' THEN 1.0 ELSE 0.0 END)) count_cmd6

FROM (SELECT * FROM y_offline_positions_from_cosmos WHERE cmd !=6)
JOIN tmp_vars ON close_time <> '1970-01-01' AND close_time < a_date  AND close_time > dateadd(day, -180, a_date)
GROUP BY customer_id
) t1
JOIN (SELECT customer_id, COUNT(*) count_cmd_future, SUM(amount) sum_amount_future FROM y_offline_positions_from_cosmos JOIN tmp_vars ON close_time <> '1970-01-01' GROUP BY customer_id) t2 ON t1.customer_id = t2.customer_id
;

"""


# def get_status(count_cmd1, count_cmd2, count_cmd3, count_cmd4, count_cmd5, count_cmd6):
#     if count_cmd1 == 0 and count_cmd2 == 0 and count_cmd3 == 0 and count_cmd4 == 0 and count_cmd5 == 0 and count_cmd6 != 0:
#             return Status.new_customer
#     elif count_cmd5 != 0:
#         if count_cmd6 != 0:
#             return Status.old_customer
#         else:
#             return Status.new_churn
#     elif count_cmd5 == 0 and count_cmd6 == 0:
#         return Status.old_churn
#     elif count_cmd6 != 0 and (count_cmd1 != 0 or count_cmd2 != 0 or
#                               count_cmd3 != 0 or count_cmd4 != 0 or
#                               count_cmd5 != 0):
#         return Status.came_back
#     else:
#         #TODO
#         return 8


def get_months_active_numbers(sum_profits1, sum_profits2, sum_profits3, sum_profits4, sum_profits5, sum_profits6):
    return sum(1 for x in [sum_profits1, sum_profits2, sum_profits3, sum_profits4, sum_profits5, sum_profits6]
               if x != 0.0)


def output_clv_one_group(customers, status, churn_rate_1, churn_rate_2, time_str):
    path_csv = os.path.join(clv_output_directory, "clv_" + status + "_" + time_str + ".csv")
    churn_rate_1_str = "months_avg_amount / " + str(churn_rate_1)
    churn_rate_2_str = "months_avg_amount / " + str(churn_rate_2)
    discount1 = 0
    discount2 = 0.1
    discount3 = 1.1
    commission = 0.001
    retention_rate_1 = 1 - churn_rate_1
    retention_rate_2 = 1 - churn_rate_2
    retention_rate_1_str = "retention = " + str(retention_rate_1) + " discount = " + str(discount1)
    retention_rate_2_str = "retention = " + str(retention_rate_2) + " discount = " + str(discount1)
    retention_rate_3_str = "retention = " + str(retention_rate_1) + " discount = " + str(discount2)
    retention_rate_4_str = "retention = " + str(retention_rate_2) + " discount = " + str(discount2)
    retention_rate_5_str = "retention = " + str(retention_rate_1) + " discount = " + str(discount3) + "_2"
    retention_rate_6_str = "retention = " + str(retention_rate_2) + " discount = " + str(discount3) + "_2"
    retention_rate_7_str = "retention = " + str(retention_rate_1) + " discount = " + str(discount2) + "_2"
    retention_rate_8_str = "retention = " + str(retention_rate_2) + " discount = " + str(discount2) + "_2"
    # write output_analytics_list
    with open(path_csv, 'wb') as csv_file:
        fieldnames = ['customer_id', 'count_cmd', 'sum_amount_commission', 'months_active',
                      'months_avg_amount_commission', 'months_avg_cmd',
                      churn_rate_1_str, churn_rate_2_str,
                      retention_rate_1_str, retention_rate_2_str,
                      retention_rate_3_str, retention_rate_4_str,
                      retention_rate_5_str, retention_rate_6_str,
                      retention_rate_7_str, retention_rate_8_str,
                      'diff_abs_' + churn_rate_1_str, 'diff_abs_' + churn_rate_2_str,
                      'diff_abs_' + retention_rate_1_str, 'diff_abs_' + retention_rate_2_str,
                      'diff_abs_' + retention_rate_3_str, 'diff_abs_' + retention_rate_4_str,
                      'diff_abs_' + retention_rate_5_str, 'diff_abs_' + retention_rate_6_str,
                      'diff_abs_' + retention_rate_7_str, 'diff_abs_' + retention_rate_8_str,
                      'count_cmd_future', 'sum_amount_future_commission']

        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for customer in customers:
            sum_amount_future_commission = commission*customer.sum_amount_future
            sum_amount_commission = commission*customer.sum_amount
            if customer.months_active != 0:
                months_avg_amount = commission*(float(customer.sum_amount) / customer.months_active)
                months_avg_cmd = float(customer.count_cmd) / customer.months_active
            else:
                months_avg_amount = 0
                months_avg_cmd = 0
            churn_rate_1_calc = sum_amount_commission + (months_avg_amount / churn_rate_1)
            churn_rate_2_calc = sum_amount_commission + (months_avg_amount / churn_rate_2)
            retention_rate_1_calc = sum_amount_commission + float(sum_amount_commission*retention_rate_1)/(1+discount1-retention_rate_1)
            retention_rate_2_calc = sum_amount_commission + float(sum_amount_commission*retention_rate_2)/(1+discount1-retention_rate_2)
            retention_rate_3_calc = sum_amount_commission + float(sum_amount_commission*retention_rate_1)/(1+discount2-retention_rate_1)
            retention_rate_4_calc = sum_amount_commission + float(sum_amount_commission*retention_rate_2)/(1+discount2-retention_rate_2)
            retention_rate_5_calc = sum_amount_commission + float(sum_amount_commission*retention_rate_1)/(1+discount1)
            retention_rate_6_calc = sum_amount_commission + float(sum_amount_commission*retention_rate_2)/(1+discount1)
            retention_rate_7_calc = sum_amount_commission + float(sum_amount_commission*retention_rate_1)/(1+discount2)
            retention_rate_8_calc = sum_amount_commission + float(sum_amount_commission*retention_rate_2)/(1+discount2)
            writer.writerow({'customer_id': str(customer.customer_id),
                             'count_cmd': str(customer.count_cmd),
                             'sum_amount_commission': str(commission*customer.sum_amount),
                             'months_active': str(customer.months_active),
                             'months_avg_amount_commission': str(months_avg_amount),
                             'months_avg_cmd': str(months_avg_cmd),
                             churn_rate_1_str: str(churn_rate_1_calc),
                             churn_rate_2_str: str(churn_rate_2_calc),
                             retention_rate_1_str: str(retention_rate_1_calc),
                             retention_rate_2_str: str(retention_rate_2_calc),
                             retention_rate_3_str: str(retention_rate_3_calc),
                             retention_rate_4_str: str(retention_rate_4_calc),
                             retention_rate_5_str: str(retention_rate_5_calc),
                             retention_rate_6_str: str(retention_rate_6_calc),
                             retention_rate_7_str: str(retention_rate_7_calc),
                             retention_rate_8_str: str(retention_rate_8_calc),
                             'diff_abs_' + churn_rate_1_str: str(abs(sum_amount_future_commission - churn_rate_1_calc)),
                             'diff_abs_' + churn_rate_2_str: str(abs(sum_amount_future_commission - churn_rate_2_calc)),
                             'diff_abs_' + retention_rate_1_str: str(abs(sum_amount_future_commission - retention_rate_1_calc)),
                             'diff_abs_' + retention_rate_2_str: str(abs(sum_amount_future_commission - retention_rate_2_calc)),
                             'diff_abs_' + retention_rate_3_str: str(abs(sum_amount_future_commission - retention_rate_3_calc)),
                             'diff_abs_' + retention_rate_4_str: str(abs(sum_amount_future_commission - retention_rate_4_calc)),
                             'diff_abs_' + retention_rate_5_str: str(abs(sum_amount_future_commission - retention_rate_5_calc)),
                             'diff_abs_' + retention_rate_6_str: str(abs(sum_amount_future_commission - retention_rate_6_calc)),
                             'diff_abs_' + retention_rate_7_str: str(abs(sum_amount_future_commission - retention_rate_7_calc)),
                             'diff_abs_' + retention_rate_8_str: str(abs(sum_amount_future_commission - retention_rate_8_calc)),
                             'count_cmd_future': str(customer.count_cmd_future),
                             'sum_amount_future_commission': str(sum_amount_future_commission)
                             })
        csv_file.close()


def simple_clv(train_date):
    query_date_string = """-- reference date table
                    DROP TABLE IF EXISTS tmp_vars;
                    CREATE temp TABLE tmp_vars (
                      a_date DATE
                    );
                    INSERT INTO tmp_vars (a_date) VALUES
                      ('%(date)s')
                     ;\n""" % {"date": train_date}

    final_query = query_date_string + sql_script
    rows = query_redshift(final_query)
    customers_1_month = []
    customers_2_months = []
    customers_3_months = []
    customers_4_months = []
    customers_5_months = []
    customers_6_months = []
    for row in rows:
        customer = Customer(row)
        if customer.months_active == 1:
            customers_1_month.append(customer)
        elif customer.months_active == 2:
            customers_2_months.append(customer)
        elif customer.months_active == 3:
            customers_3_months.append(customer)
        elif customer.months_active == 4:
            customers_4_months.append(customer)
        elif customer.months_active == 5:
            customers_5_months.append(customer)
        elif customer.months_active == 6:
            customers_6_months.append(customer)

    myLog.log('customers_1_month = ' + str(len(customers_1_month)))
    myLog.log('customers_2_months = ' + str(len(customers_2_months)))
    myLog.log('customers_3_months = ' + str(len(customers_3_months)))
    myLog.log('customers_4_months = ' + str(len(customers_4_months)))
    myLog.log('customers_5_months = ' + str(len(customers_5_months)))
    myLog.log('customers_6_months = ' + str(len(customers_6_months)))

    # *0.88 because 12% of customers return after they churned
    # churn_rate_1 = float(len(customers_new_churn)*0.88) / (len(customers_old_customers) + len(customers_new_churn) + len(customers_new_customers))
    # churn_rate_2 = float(len(customers_new_churn)*0.88) / (len(customers_old_customers) + len(customers_new_churn) + len(customers_new_customers) + len(customers_came_back))
    # myLog.log('churn_rate_1 = ' + str(churn_rate_1))
    # myLog.log('churn_rate_2 = ' + str(churn_rate_2))

    now = datetime.datetime.now()
    time_str = now.strftime("%Y%m%dT%H%M%S")

    # output_clv_one_group(customers_1_month, "customers_1_month", 0.6, 0.71, time_str)
    # output_clv_one_group(customers_2_months, "customers_2_months", 0.54, 0.71, time_str)
    # output_clv_one_group(customers_3_months, "customers_3_months", 0.31, 0.46, time_str)
    # output_clv_one_group(customers_4_months, "customers_4_months", 0.2, 0.31, time_str)
    # output_clv_one_group(customers_5_months, "customers_5_months", 0.13, 0.2, time_str)
    # output_clv_one_group(customers_6_months, "customers_6_months", 0.04, 0.09, time_str)

    output_clv_one_group(customers_1_month, "customers_1_month", 0.6*0.26*0.25*0.4*0.53*0.63, 0.71*0.26*0.25*0.4*0.53*0.63, time_str)
    output_clv_one_group(customers_2_months, "customers_2_months", 0.54*0.25*0.4*0.53*0.63, 0.71*0.25*0.4*0.53*0.63, time_str)
    output_clv_one_group(customers_3_months, "customers_3_months", 0.31*0.4*0.53*0.63, 0.46*0.4*0.53*0.63, time_str)
    output_clv_one_group(customers_4_months, "customers_4_months", 0.2*0.53*0.63, 0.31*0.53*0.63, time_str)
    output_clv_one_group(customers_5_months, "customers_5_months", 0.13*0.63, 0.2*0.63, time_str)
    output_clv_one_group(customers_6_months, "customers_6_months", 0.04*0.63, 0.09*0.63, time_str)


#simple_clv('2015-10-01')
