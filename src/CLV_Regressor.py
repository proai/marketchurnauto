import csv
import os

import datetime
import numpy as np
import math

import pickle
import pylab as pl

from numpy.polynomial.polynomial import polyfit
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.tree import DecisionTreeRegressor

import myLog
from generic_function import load_data_np
from getClassifiers import load_classifier


def get_list_of_files(read_me_file, test_size=1, market_directory="", cosmos_directory=""):
    f = open(read_me_file, 'r')
    file_txt = f.read().split('\n')
    final_file_txt = []
    for a_line in file_txt:
        if len(a_line) < 2 or '#' in a_line:
            myLog.log('skipping: ' + a_line)
        else:
            # add relevant file for market
            if market_directory is not "":
                path = os.path.join(os.path.join(clv_directory, market_directory), a_line)
                final_file_txt.append(path)
                myLog.log('adding: ' + a_line)
            # add relevant file for cosmos
            if cosmos_directory is not "":
                path = os.path.join(os.path.join(clv_directory, cosmos_directory), a_line)
                final_file_txt.append(path)
                myLog.log('adding: ' + a_line)
            else:
                myLog.log('skipping: ' + a_line)

    train_list = final_file_txt[:-test_size]
    test_list = final_file_txt[-test_size:]
    sub_output_analytics = ["Train by: " + str(train_list), "Test by: " + str(test_list)]
    myLog.log('Train by: ' + str(train_list))
    myLog.log('Test by: ' + str(test_list))
    return train_list, test_list, sub_output_analytics


def create_data_async(market_directory="", cosmos_directory=""):
    test_size = 1
    read_me_file = os.path.join(resources_directory, "clv_files.txt")
    data_train_list, data_tst_list, sub_output_analytics = get_list_of_files(read_me_file, test_size,
                                                                             market_directory, cosmos_directory)

    data_train = load_data_np(data_train_list)
    data_tst = load_data_np(data_tst_list)

    return data_train, data_tst, sub_output_analytics


def create_data_async_90(market_directory="", cosmos_directory=""):
    test_size = 1
    read_me_file = os.path.join(resources_directory, "clv_files.txt")
    data_train_list, data_tst_list, sub_output_analytics = get_list_of_files(read_me_file, test_size,
                                                                             market_directory, cosmos_directory)

    data_train = load_data_np(data_train_list)
    data_tst = load_data_np(data_tst_list)

    return data_train, data_tst, sub_output_analytics, data_train_list, data_tst_list

###############################################################
# paths                                                       #
###############################################################
src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr) - 1])
resources_directory = os.path.join(project_directory, "resources")
parameters_file = os.path.join(resources_directory, "parameters.txt")
log_file = os.path.join(os.path.join(project_directory, "logs"), "log.txt")
tmp_directory = os.path.join(project_directory, "tmp")
data_directory = os.path.join(project_directory, "data")
clv_directory = os.path.join(data_directory, "CLV")
output_directory = os.path.join(project_directory, "output")
classifiers_directory = os.path.join(output_directory, "classifiers")
classifier_path = os.path.join(classifiers_directory, "30_clf_random_forest_08.p")

###############################################################
# parameters                                                  #
###############################################################
paths_of_files_to_remove = []
label_col = 0
customers_col = 1
min_data_col = 2


def save_csv_regression(customers_ids, test_y, regressor_name, prediction, predict_random_forest_churn, time_str):
    regression_path = os.path.join(output_directory, "regression")
    regression_path_csv = os.path.join(regression_path, regressor_name + "_regression_" + time_str + ".csv")
    # write output_analytics_list
    with open(regression_path_csv, 'wb') as csv_file:
        fieldnames = ['customers ids', 'churn', 'test y', 'prediction',
                      'diff_test_y_prediction']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for i in range(0, len(customers_ids)):
            writer.writerow({'customers ids': str(customers_ids[i]),
                             'churn': str(predict_random_forest_churn[i]),
                             'test y': str(test_y[i]),
                             'prediction': str(prediction[i]),
                             'diff_test_y_prediction': str(test_y[i]-prediction[i])
                             })
        csv_file.close()


def calculate_clv(market_directory="", cosmos_directory=""):
    # data_train, data_tst, sub_output_analytics = create_data_async(market_directory, cosmos_directory)
    data_train, data_tst, sub_output_analytics, data_train_list, data_tst_list = create_data_async_90(market_directory, cosmos_directory)

    # train_x = data_train[:, min_data_col:]
    # train_y = data_train[:, label_col]
    # test_x = data_tst[:, min_data_col:]
    # test_y = data_tst[:, label_col]
    # customers_ids = data_tst[:, customers_col]

    train_x_1 = data_tst[:, min_data_col:]
    train_y_1 = data_tst[:, label_col]
    train_x = train_x_1[:int(len(train_x_1)*0.8)]
    test_x = train_x_1[int(len(train_x_1)*0.8):]
    train_y = train_y_1[:int(len(train_y_1)*0.8)]
    test_y = train_y_1[int(len(train_y_1)*0.8):]
    customers_ids = (data_tst[:, customers_col])[int(len(train_y_1)*0.8):]

    sub_output_analytics.append('The length of train data: ' + str(len(data_train)))
    sub_output_analytics.append('The length of data_tst: ' + str(len(data_tst)))

    predictions = []

    # LinearRegression
    ############################
    # give negative values...
    if False:
        lr = LinearRegression()
        lr.fit(train_x, train_y)
        lr_predict_for_train = lr.predict(train_x)
        lr_predict_for_test = lr.predict(test_x)
        score_train = lr.score(train_x, train_y)
        score_test = lr.score(test_x, test_y)
        sub_output_analytics.append('LR Score on train_x, train_y: ' + str(score_train))
        sub_output_analytics.append('LR Score on test_x, test_y: ' + str(score_test))
        lr_dif_train_y_predict_train = train_y - lr_predict_for_train
        lr_dif_test_y_predict_test = test_y - lr_predict_for_test
        sub_output_analytics.append('LR Average (|train_y - predict_for_train|): ' + str(np.mean([abs(x) for x in lr_dif_train_y_predict_train])))
        sub_output_analytics.append('LR Average (|test_y - predict_for_test|): ' + str(np.mean([abs(x) for x in lr_dif_test_y_predict_test])))
        lr_standard_dev_train = math.sqrt(np.var(lr_dif_train_y_predict_train))
        lr_standard_dev_test = math.sqrt(np.var(lr_dif_test_y_predict_test))
        sub_output_analytics.append('LR Standard dev (train_y - predict_for_train): ' + str(lr_standard_dev_train))
        sub_output_analytics.append('LR Standard dev (test_y - predict_for_test): ' + str(lr_standard_dev_test))
        predictions.append(("lr", lr_predict_for_test))

    # DecisionTreeRegressor
    ############################
    if True:
        dtr = DecisionTreeRegressor()
        dtr.fit(train_x, train_y)
        dtr_predict_for_train = dtr.predict(train_x)
        dtr_predict_for_test = dtr.predict(test_x)
        dtr_dif_train_y_predict_train = train_y - dtr_predict_for_train
        dtr_dif_test_y_predict_test = test_y - dtr_predict_for_test
        sub_output_analytics.append('DTR Average (|train_y - predict_for_train|): ' + str(np.mean([abs(x) for x in dtr_dif_train_y_predict_train])))
        sub_output_analytics.append('DTR Average (|test_y - predict_for_test|): ' + str(np.mean([abs(x) for x in dtr_dif_test_y_predict_test])))
        standard_dev_train = math.sqrt(np.var(dtr_dif_train_y_predict_train))
        standard_dev_test = math.sqrt(np.var(dtr_dif_test_y_predict_test))
        sub_output_analytics.append('DTR Standard dev (train_y - predict_for_train): ' + str(standard_dev_train))
        sub_output_analytics.append('DTR Standard dev (test_y - predict_for_test): ' + str(standard_dev_test))
        predictions.append(("dtr", dtr_predict_for_test))

    # SVR
    ############################
    if False:
        svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)
        # svr_lin = SVR(kernel='linear', C=1e3)
        # svr_poly = SVR(kernel='poly', C=1e3, degree=2)
        # y_lin = svr_lin.fit(train_x, train_y).predict(train_x)
        # y_poly = svr_poly.fit(train_x, train_y).predict(train_x)
        svr_rbf.fit(train_x, train_y)
        rbf_predict_for_train = svr_rbf.predict(train_x)
        rbf_predict_for_test = svr_rbf.predict(test_x)
        rbf_dif_train_y_predict_train = train_y - rbf_predict_for_train
        rbf_dif_test_y_predict_test = test_y - rbf_predict_for_test
        sub_output_analytics.append('SVR RBF Average (|train_y - predict_for_train|): ' + str(np.mean([abs(x) for x in rbf_dif_train_y_predict_train])))
        sub_output_analytics.append('SVR RBF Average (|test_y - predict_for_test|): ' + str(np.mean([abs(x) for x in rbf_dif_test_y_predict_test])))
        standard_dev_train = math.sqrt(np.var(rbf_dif_train_y_predict_train))
        standard_dev_test = math.sqrt(np.var(rbf_dif_test_y_predict_test))
        sub_output_analytics.append('SVR RBF Standard dev (train_y - predict_for_train): ' + str(standard_dev_train))
        sub_output_analytics.append('SVR RBF Standard dev (test_y - predict_for_test): ' + str(standard_dev_test))
        predictions.append(("rbf", dtr_predict_for_test))

    # if False:
    #     pl.scatter(train_x, train_y, c='k', label='data')
    #     pl.hold('on')
    #     pl.plot(train_x, y_rbf, c='g', label='RBF model')
    #     # pl.plot(X, y_lin, c='r', label='Linear model')
    #     # pl.plot(X, y_poly, c='b', label='Polynomial model')
    #     pl.xlabel('data')
    #     pl.ylabel('target')
    #     pl.title('Support Vector Regression')
    #     pl.legend()
    #     pl.show()

    clf_random_forest = pickle.load(open(classifier_path, "rb"))
    predict_random_forest = clf_random_forest.predict_proba(test_x)
    predict_random_forest_churn = predict_random_forest[:, 1]

    # print output_analytics
    now = datetime.datetime.now()
    time_str = now.strftime("%Y%m%dT%H%M%S")
    regression_path = os.path.join(output_directory, "regression")
    regression_path_txt = os.path.join(regression_path, "regression_" + time_str + ".txt")
    # write sub_output_analytics
    f = open(regression_path_txt, 'w')
    for line in sub_output_analytics:
        f.write(line + "\n")
    f.close()
    for (regressor_name, prediction) in predictions:
        save_csv_regression(customers_ids, test_y, regressor_name, prediction, predict_random_forest_churn, time_str)


if __name__ == '__main__':
    calculate_clv("", "cosmos")
