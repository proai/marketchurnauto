import os
from os import listdir
from os.path import isfile, join
from multiprocessing import Process
import myLog
from generic_function import get_file_according_columns
from getClassifiers import calculate_classifiers, find_optimal_parameters
from getFeatures import get_feature_data, rewrite_all_files
from redshift import query_redshift

src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr) - 1])
home_directory_output = os.path.join(project_directory, "output")
resources_directory = os.path.join(project_directory, "resources")
classifiers_directory = os.path.join(home_directory_output, "classifiers")
output_prediction_directory = os.path.join(project_directory, "output_prediction")


################################################################

def create_and_load_classifier(p_interval, p_first_date, p_last_date, p_thresholds, p_n_estimators, p_max_features,
                               p_min_samples_leaf, p_max_depth, p_check_params_effectiveness, p_show_graph,
                               p_train_all_files_in_list, p_market_directory="", p_cosmos_directory=""):

    rewrite_all_files(str(p_interval), p_first_date, p_last_date)
    myLog.log("calculate classifiers for interval of " + str(p_interval) + " days")
    classifier_name = calculate_classifiers(p_interval, p_thresholds, p_n_estimators, p_max_features,
                                            p_min_samples_leaf, p_max_depth, p_check_params_effectiveness, p_show_graph,
                                            p_train_all_files_in_list, p_market_directory, p_cosmos_directory)
    return classifier_name


def get_newest_file(interval):
    only_files = [os.path.join(home_directory_output, f) for f in listdir(home_directory_output)
                  if isfile(join(home_directory_output, f)) and "optimal" in f and "csv" in f and interval in f]
    newest = max(only_files, key=os.path.getctime)
    return newest


def to_value(param):
    if param is None:
        return None
    if param.strip() == 'None':
        return None
    try:
        return int(param)
    except ValueError:
        return param


def get_optimal_parameters(interval):
    file_name = get_newest_file(interval)
    columns = get_file_according_columns(file_name)
    max_avg_precision_recall = max(columns['Precision & Recall avg'])
    index = columns['Precision & Recall avg'].index(max_avg_precision_recall)
    return (to_value(columns['Number of trees in random forest'][index]),
            to_value(columns['Min samples per leaf in random forest'][index]),
            to_value(columns['Max features per tree'][index]),
            to_value(columns['Max depth for random forest'][index]))


def output_ready():
    f = open(os.path.join(output_prediction_directory, "ready"), 'w')
    f.close()


def api_get_prediction(intervals_days, first_date, last_date, thresholds=[0.5, 0.6, 0.7], check_max_depth=[20, None],
                       check_n_estimators=[20, 30], check_min_sample_leaf=[5, 10], check_max_features=[35],
                       check_params_effectiveness=False, show_graph=False, train_all_files_in_list=True,
                       cosmos_directory="y_cosmos", cosmos_table=" y_offline_positions_from_cosmos  ",
                       market_directory="", market_table=""):
    ################################################################

    ##################################
    # create data                    #
    ##################################
    if market_table is not "" and market_directory is not "":
        myLog.log("calculate data for market")
        get_feature_data(intervals_days, market_table, market_directory, first_date, last_date)
    if cosmos_table is not "" and cosmos_directory is not "":
        myLog.log("calculate data for cosmos")
        get_feature_data(intervals_days, cosmos_table, cosmos_directory, first_date, last_date)

    ##################################
    # find optimal parameters        #
    ##################################
    processes = []
    for interval in intervals_days:
        rewrite_all_files(str(interval), first_date, last_date)
        process = Process(target=find_optimal_parameters,
                          args=(interval, market_directory, cosmos_directory, thresholds, check_max_depth,
                                check_n_estimators, check_min_sample_leaf, check_max_features,))
        process.start()
        processes.append(process)
    for process in processes:
        process.join()

    ##################################
    # calculate predictions          #
    ##################################
    processes = []
    for interval in intervals_days:
        n_estimators, min_samples_leaf, max_features, max_depth = get_optimal_parameters(interval)
        myLog.log('Find optimal parameters: n_estimators = ' + str(n_estimators) + ', min_samples_leaf = ' +
                  str(min_samples_leaf) + ', max_features = ' + str(max_features) + ', max_depth = ' +
                  str(max_depth))
        process = Process(target=create_and_load_classifier,
                          args=(interval, first_date, last_date, thresholds, n_estimators, max_features,
                                min_samples_leaf, max_depth, check_params_effectiveness, show_graph,
                                train_all_files_in_list, market_directory, cosmos_directory,))
        process.start()
        processes.append(process)
    for process in processes:
        process.join()

    output_ready()


def get_last_date_arg():
    sql_query = "select top 1 close_time from y_offline_positions_from_cosmos order by close_time desc;"
    last_date = query_redshift(sql_query)
    return str(last_date[0][0]).split(" ")[0]


if __name__ == '__main__':

    intervals_file_name = os.path.join(resources_directory, "intervals.txt")
    first_date_file_name = os.path.join(resources_directory, "first_date.txt")
    intervals_days_arg = [line.strip() for line in open(intervals_file_name, 'r')]
    first_last_date_from_file = [line.strip() for line in open(first_date_file_name, 'r')]
    first_date_arg = first_last_date_from_file[0]
    last_date_from_file = False
    if len(first_last_date_from_file) > 1:
        if first_last_date_from_file[1].count("-") == 2:
            last_date_arg = first_last_date_from_file[1]
            last_date_from_file = True
    if not last_date_from_file:
        last_date_arg = get_last_date_arg()
    api_get_prediction(intervals_days_arg, first_date_arg, last_date_arg)

