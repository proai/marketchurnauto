from concurrent import futures
from concurrent.futures import ProcessPoolExecutor
import numpy as np
import myLog
import csv
from collections import defaultdict


def load_data_np(data_list):
    ##################################
    # ProcessPoolExecutor            #
    ##################################
    with ProcessPoolExecutor() as executor:
        fs = [executor.submit(read_data_from_file, data_path) for data_path in data_list]
        futures.wait(fs)
        results = [f.result() for f in fs]
    data = []
    for result in results:
        for line in result:
            data.append(line)
    data_np = np.loadtxt(data, delimiter=',')
    return data_np


def read_data_from_file(data_path):
    myLog.log(data_path)
    f = file(data_path, "r")
    return f.readlines()


def get_file_according_columns(file_name):
    # each value in each column is appended to a list
    columns = defaultdict(list)
    with open(file_name) as f:
        # read rows into a dictionary format
        reader = csv.DictReader(f)
        # read a row as {column1: value1, column2: value2,...}
        for row in reader:
            # go over each column name and value
            for (k, v) in row.items():
                columns[k].append(v)
    return columns
