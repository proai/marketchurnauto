import csv
import random
from multiprocessing import Process, Manager
import time
import sys

import itertools
from concurrent import futures
from concurrent.futures import ProcessPoolExecutor
from sklearn.ensemble import RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import *
from sklearn import metrics, svm
import numpy as np
import pylab as pl
import xlsxwriter
import datetime
import os
import pickle
import myLog
from generic_function import load_data_np
from prediction import to_json, save_to_excel


def get_list_of_files(read_me_file, train_all_files_in_list, interval, train_limit=1, test_size=1,
                      market_directory="", cosmos_directory=""):
    f = open(read_me_file, 'r')
    file_txt = f.read().split('\n')
    final_file_txt = []
    for a_line in file_txt:
        if len(a_line) < 2 or '#' in a_line:
            myLog.log('skipping: ' + a_line)
        else:
            interval_num = str(a_line).split("_")[0]
            if str(interval_num) == str(interval):
                # add relevant file for market
                if market_directory is not "":
                    path = os.path.join(os.path.join(data_directory, market_directory), a_line)
                    final_file_txt.append(path)
                    myLog.log('adding: ' + a_line)
                # add relevant file for cosmos
                if cosmos_directory is not "":
                    path = os.path.join(os.path.join(data_directory, cosmos_directory), a_line)
                    final_file_txt.append(path)
                    myLog.log('adding: ' + a_line)
            else:
                myLog.log('skipping: ' + a_line)
    if train_all_files_in_list:
        train_list = final_file_txt[:]
        test_list = final_file_txt[-test_size:]
    else:
        # If the training data compose from market + cosmos then there will be 2 test files for the last month.
        if market_directory is not "" and cosmos_directory is not "":
            train_list = final_file_txt[:-(train_limit + 1)]
            test_list = final_file_txt[-(test_size + 1):]
            # test on market & cosmos
            # train_list.append(test_list[0])
            # test only on cosmos
            test_list = [test_list[1]]
        else:
            train_list = final_file_txt[:-train_limit]
            test_list = final_file_txt[-test_size:]
    sub_output_analytics = ["Train by: " + str(train_list), "Test by: " + str(test_list)]
    myLog.log('Train by: ' + str(train_list))
    myLog.log('Test by: ' + str(test_list))
    return train_list, test_list, sub_output_analytics


def precision_and_recall(predict, actual, threshold=0.5):
    """    2016-01-26:
    :rtype: object
    :param predict: array of floats. the probability of the relevant example to be of positive label by a model that is
        checked
    :param actual: actual labels in the validation set
    :param threshold: if the predicted probability in predict is > threshold, the expected results is 1, otherwise 0
    :return: Precision and recall of the prediction
    """
    tp, tn, fp, fn = 0.0, 0.0, 0.0, 0.0
    if len(predict) != len(actual):
        print "!!! ERR: precision_and_recall: len(predict) != len(actual)"
        return 0, 0
    for i in xrange(len(predict)):
        if predict[i] >= threshold:
            if actual[i] == 1:
                tp += 1.0
            else:
                fp += 1.0
        else:
            if actual[i] == 1:
                fn += 1.0
            else:
                tn += 1.0
    if tp == 0:
        print "!!! ERR: precision_and_recall - tp ==0"
        return 0, 0
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    return precision, recall


def output_analytics_to_file(classifier_name, output_analytics, interval, time_str):
    file_name = interval + "_" + classifier_name + "_output_analytic_" + time_str + ".txt"
    f = open(os.path.join(output_directory, file_name), 'w')
    for line in output_analytics:
        f.write(line + "\n")


def param_effectiveness(classifier, data_test, param_inx, iterations=1000):
    """
    2016-01-21. Originally created to check the effectiveness of parameters in the churn model for Markets.
    We'll shuffle the values of this parameter between all rows in the test set. And compare it with the results
    given on the original data.
    :param classifier: The classifier to check
    :param data_test: The test set.
    :param param_inx: Index of the relent param.
    :param iterations: Number of checks to make. The output will be the avg result
    :return: The avg difference between the original results, and the results after shuffling the parameter.
    """
    predict_original = classifier.predict_proba(data_test)
    predict_original_1 = predict_original[:, 1]
    a = 0.0
    for i in xrange(iterations):
        alter_data = np.copy(data_test)
        np.random.shuffle(alter_data[:, param_inx])
        predict_shuffle = classifier.predict_proba(alter_data)
        predict_shuffle_1 = predict_shuffle[:, 1]
        a += sum(abs(predict_original_1 - predict_shuffle_1)) / len(predict_original_1)
    return a / iterations


def all_params_effectiveness(classifier, classifier_name, test_data, params_names, interval, time_str, iterations=1000):
    """
    2016-04-21. Using param_effectiveness.
    Giving the effectiveness of all parameters.
    :param time_str:
    :param classifier_name:
    :param interval:
    :param classifier: classifier that is checked
    :param test_data:
    :param params_names: array of strings. in inx i the name of param i
    :param iterations: for each param, we'll make iterations amount of checks
    :return:
    """
    effective_pairs = []
    for inx in xrange(len(params_names)):
        effectiveness = param_effectiveness(classifier=classifier, data_test=test_data, param_inx=inx,
                                            iterations=iterations)
        effective_pairs.append((effectiveness, params_names[inx]))
    effective_pairs = sorted(effective_pairs, key=lambda s: s[0], reverse=True)
    output_param_effectiveness(classifier_name, effective_pairs, interval, time_str)


def output_param_effectiveness(classifier_name, effective_pairs, interval, time_str):
    parameters_effectiveness_path = os.path.join(output_directory,
                                                 interval + "_" + str(classifier_name) + "_parameters_effectiveness_" +
                                                 time_str + ".xlsx")
    workbook = xlsxwriter.Workbook(parameters_effectiveness_path)
    worksheet = workbook.add_worksheet()
    # Write some simple text.
    worksheet.set_column('A:A', 60)
    worksheet.set_column('B:B', 15)
    bold = workbook.add_format({'bold': True})
    worksheet.write('A1', 'Parameters', bold)
    worksheet.write('B1', 'Effectiveness', bold)
    row_index = 1
    for pair in effective_pairs:
        worksheet.write(row_index, 0, pair[1])
        worksheet.write(row_index, 1, pair[0])
        row_index += 1
    workbook.close()
    myLog.log("parameters effectiveness, file_path: " + parameters_effectiveness_path)


# def remove_files_list():
#     myLog.log('removing files: ' + str(paths_of_files_to_remove))
#     for file1 in paths_of_files_to_remove:
#         try:
#             myLog.log("removing: " + str(file1))
#             os.remove(file1)
#         except OSError:
#             myLog.log('Exception: ' + file1 + ' does not EXISTS')


def output_prediction(classifier_name, predict_random_forest, customers_ids, interval, time_str):
    customers_prediction = to_json(predict_random_forest, customers_ids)
    myLog.log("write prediction of " + classifier_name + " to file")
    save_to_excel(classifier_name, customers_prediction, str(interval), time_str)
    return customers_prediction


def create_data_async(interval, market_directory="", cosmos_directory="", train_all_files_in_list=False):
    all_files_path = os.path.join(resources_directory, str(interval) + "_all_files.txt")
    if train_all_files_in_list:
        # training data is compose from all files
        # there is an overlap with the test file!
        data_train_list, data_tst_list, sub_output_analytics = get_list_of_files(all_files_path,
                                                                                 train_all_files_in_list,
                                                                                 interval, 0, 1,
                                                                                 market_directory, cosmos_directory)
    else:
        # training data is compose from all files but one
        # there is no overlap!
        data_train_list, data_tst_list, sub_output_analytics = get_list_of_files(all_files_path,
                                                                                 train_all_files_in_list,
                                                                                 interval, 1, 1,
                                                                                 market_directory, cosmos_directory)

    data_train = load_data_np(data_train_list)
    data_tst = load_data_np(data_tst_list)

    return data_train, data_tst, sub_output_analytics


def calculate_classifiers(interval, thresholds, n_estimators=30, max_features=35, min_samples_leaf=10, max_depth=20,
                          check_params_effectiveness=False, show_graph=False, train_all_files_in_list=False,
                          market_directory="", cosmos_directory=""):
    ##################################
    # create data                    #
    ##################################
    data_train, data_tst, sub_output_analytics = create_data_async(interval, market_directory, cosmos_directory,
                                                                   train_all_files_in_list)
    train_x = data_train[:, min_data_col:]
    train_y = data_train[:, label_col]
    test_x = data_tst[:, min_data_col:]
    test_y = data_tst[:, label_col]
    customers_ids = data_tst[:, customers_col]
    tst_rate = round(float(sum(test_y)) / float(len(test_y)), 2)
    train_rate = round(float(sum(train_y)) / float(len(train_y)), 2)
    sub_output_analytics.append('The length of train data: ' + str(len(data_train)))
    sub_output_analytics.append('The length of data_tst: ' + str(len(data_tst)))
    sub_output_analytics.append('Number of positive in train: '+str(sum(train_y))+', positive rate: ' + str(train_rate))
    sub_output_analytics.append('Number of positive in test: ' + str(sum(test_y)) + ', positive rate: ' + str(tst_rate))

    ##################################
    # create classifier              #
    ##################################

    # warm_start : bool, optional (default=False)
    # When set to ``True``, reuse the solution of the previous call to fit
    # and add more estimators to the ensemble, otherwise, just fit a whole
    # new forest.

    # default values
    # n_estimators = 10
    # min_samples_leaf = 1
    # max_features = "auto"
    # max_depth = None

    # optimal values
    # n_estimators = 30
    # min_samples_leaf = 10
    # max_features = 35
    # max_depth = 20

    random_seed = random.randint(1, sys.maxint)
    clf_random_forest = RandomForestClassifier(n_estimators=n_estimators,
                                               criterion="gini",
                                               max_depth=max_depth,
                                               min_samples_split=2,
                                               min_samples_leaf=min_samples_leaf,
                                               min_weight_fraction_leaf=0.,
                                               max_features=max_features,
                                               max_leaf_nodes=None,
                                               bootstrap=True,
                                               oob_score=False,
                                               n_jobs=1,
                                               random_state=random_seed)

    ##################################
    # processes                      #
    ##################################
    processes = []
    process = Process(target=process_classifier,
                      args=(clf_random_forest, "clf_random_forest", train_x, train_y, test_x, test_y,
                            customers_ids, interval, sub_output_analytics, thresholds, max_depth, n_estimators,
                            min_samples_leaf, max_features, check_params_effectiveness, show_graph, random_seed,))

    process.start()
    processes.append(process)

    # clf_lr = LogisticRegression()
    # process = Process(target=process_classifier,
    #                   args=(clf_lr, "clf_lr", train_x, train_y, test_x, test_y,
    #                         customers_ids, interval, sub_output_analytics, threshold,
    #                         check_params_effectiveness, show_graph, None,))
    # process.start()
    # processes.append(process)
    for process in processes:
        process.join()

    # clf_svm = svm.SVC(C=1.0,
    #                   kernel='rbf',
    #                   degree=3,
    #                   gamma='auto',
    #                   coef0=0.0,
    #                   shrinking=True,
    #                   probability=True)

    # remove_files_list()

    # classifiers_names = [file_name_random_forest, file_name_logistic_regression]
    # return classifiers_names


def save_roc_graph(classifier_name, test_y, predict_random_forest, roc_auc_rf, interval, time_str):
        fpr, tpr, thresholds = roc_curve(test_y, predict_random_forest[:, 1])
        pl.clf()
        pl.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % roc_auc_rf)
        pl.plot([0, 1], [0, 1], 'k--')
        pl.xlim([0.0, 1.0])
        pl.ylim([0.0, 1.0])
        pl.xlabel('False Positive Rate')
        pl.ylabel('True Positive Rate')
        pl.title('Receiver Operating Characteristic')
        pl.legend(loc="lower right")
        # pl.show()
        pl.savefig(os.path.join(output_directory, str(interval) + "_" + classifier_name + "_roc_curve_" +
                                time_str + ".png"))


def output_param_effectiveness_file(classifier, classifier_name, test_x, interval, time_str):
    lines = list(open(parameters_file, 'r'))
    params_names = [line.rstrip('\n') for line in lines]
    # param_effectiveness(classifier, test_x, 1)
    all_params_effectiveness(classifier, classifier_name, test_x, params_names, str(interval), time_str, 10)


def load_classifier(classifier_path, interval, data_tst_list, thresholds):
    clf_random_forest = pickle.load(open(classifier_path, "rb"))
    data_tst = load_data_np(data_tst_list)
    test_x = data_tst[:, min_data_col:]
    test_y = data_tst[:, label_col]
    customers_ids = data_tst[:, customers_col]
    predict_random_forest = clf_random_forest.predict_proba(test_x)
    output_analytics = ['Load: ' + classifier_path, "Test by: " + str(data_tst_list)]
    roc_auc_rf = metrics.roc_auc_score(test_y, predict_random_forest[:, 1])
    output_analytics.append('the random forest auc: ' + str(roc_auc_rf))
    # thresholds
    for threshold in thresholds:
        ##################################
        # Precision & Recall             #
        ##################################
        precision, recall = precision_and_recall(predict=predict_random_forest[:, 1], actual=test_y, threshold=threshold)
        output_analytics.append('Precision for threshold ' + str(threshold) + ': ' + str(precision))
        output_analytics.append('Recall for threshold ' + str(threshold) + ': ' + str(recall))
    # print output_analytics
    now = datetime.datetime.now()
    time_str = now.strftime("%Y%m%dT%H%M%S")
    output_analytics_to_file("RandomForest", output_analytics, str(interval), time_str)
    ##################################
    # output prediction              #
    ##################################
    myLog.log("calculate prediction for interval = " + str(interval))
    predict_random_forest = clf_random_forest.predict_proba(test_x)
    return output_prediction("RandomForest", predict_random_forest, customers_ids, interval, time_str)
    # remove_files_list()


def find_optimal_parameters(interval, market_directory, cosmos_directory, thresholds=None, check_max_depth=None,
                            check_n_estimators=None, check_min_sample_leaf=None, check_max_features=None):
    check_params_effectiveness = False
    show_graph = False
    ##################################
    # create data                    #
    ##################################
    data_train, data_tst, sub_output_analytics = create_data_async(interval, market_directory, cosmos_directory, False)
    train_x = data_train[:, min_data_col:]
    train_y = data_train[:, label_col]
    test_x = data_tst[:, min_data_col:]
    test_y = data_tst[:, label_col]
    # customers_ids = data_tst[:, customers_col]
    tst_rate = round(float(sum(test_y)) / float(len(test_y)), 2)
    train_rate = round(float(sum(train_y)) / float(len(train_y)), 2)
    sub_output_analytics.append('The interval is: ' + str(interval))
    sub_output_analytics.append('The length of train data: ' + str(len(data_train)))
    sub_output_analytics.append('The length of data_tst: ' + str(len(data_tst)))
    sub_output_analytics.append('Number of positive in train: '+str(sum(train_y))+', positive rate: ' + str(train_rate))
    sub_output_analytics.append('Number of positive in test: ' + str(sum(test_y)) + ', positive rate: ' + str(tst_rate))

    # max_depth parameter
    if check_max_depth is not None:
        # max_depth_array = [10, 15, 20, None]
        max_depth_array = check_max_depth
    else:
        max_depth_array = [None]
    # number of trees in the forest
    if check_n_estimators is not None:
        # n_estimators_array = [10, 20, 30]
        n_estimators_array = check_n_estimators
    else:
        n_estimators_array = [10]
    if check_min_sample_leaf is not None:
        # min_sample_leaf_array = [1, 5, 10]
        min_sample_leaf_array = check_min_sample_leaf
    else:
        min_sample_leaf_array = [1]
    if check_max_features is not None:
        # max_features_array = ["auto", 20, 35]
        max_features_array = check_max_features
        # max_features_array = [50]
    else:
        max_features_array = ["auto"]
    if thresholds is not None:
        threshold_array = thresholds
    else:
        threshold_array = [0.5, 0.6, 0.7]

    arrays = [max_depth_array, n_estimators_array, min_sample_leaf_array, max_features_array, threshold_array]
    all_possibles = list(itertools.product(*arrays))

    ##################################
    # ProcessPoolExecutor            #
    ##################################
    with ProcessPoolExecutor() as executor:
        fs = [executor.submit(find_optimal_parameters_helper,
                              train_x, train_y, test_x, test_y, interval,
                              threshold, n_estimators, max_depth, min_samples_leaf, max_features,
                              check_params_effectiveness, show_graph)
              for (max_depth, n_estimators, min_samples_leaf, max_features, threshold) in all_possibles]
        futures.wait(fs)
        results = [f.result() for f in fs]
        print_output_analytics_list(results, sub_output_analytics, interval)
        # remove_files_list()


def find_optimal_parameters_helper(train_x, train_y, test_x, test_y, interval, threshold,
                                   n_estimators, max_depth, min_samples_leaf, max_features,
                                   check_params_effectiveness, show_graph):
    ##################################
    # create classifier              #
    ##################################
    # warm_start : bool, optional (default=False)
    # When set to ``True``, reuse the solution of the previous call to fit
    # and add more estimators to the ensemble, otherwise, just fit a whole
    # new forest.
    random_seed = random.randint(1, sys.maxint)
    clf_random_forest = RandomForestClassifier(n_estimators=n_estimators,
                                               criterion="gini",
                                               max_depth=max_depth,
                                               min_samples_split=2,
                                               min_samples_leaf=min_samples_leaf,
                                               min_weight_fraction_leaf=0.,
                                               max_features=max_features,
                                               max_leaf_nodes=None,
                                               bootstrap=True,
                                               oob_score=False,
                                               n_jobs=1,
                                               random_state=random_seed)

    ##################################
    # fit                            #
    ##################################
    clf_random_forest.fit(train_x, train_y)
    ##################################
    # serialize the classifiers      #
    ##################################
    now = datetime.datetime.now()
    time_str = now.strftime("%Y%m%dT%H%M%S")
    # file_name = str(interval) + "_clf_random_forest_" + time_str + ".p"
    # pickle.dump(clf_random_forest, open(os.path.join(classifiers_directory, file_name), "wb"))
    predict = clf_random_forest.predict_proba(test_x)
    output_analytics = calculate_analytics(clf_random_forest, "clf_random_forest", predict, test_x,
                                           test_y, interval, [], [threshold],
                                           time_str, max_depth, n_estimators, min_samples_leaf,
                                           max_features, check_params_effectiveness,
                                           show_graph, random_seed)
    # return_dict[process_num] = output_analytics
    myLog.log("Process with parameters:" +
              " max_depth = " + str(max_depth) +
              " n_estimators = " + str(n_estimators) +
              " min_samples_leaf = " + str(min_samples_leaf) +
              " max_features = " + str(max_features) +
              " threshold = " + str(threshold) +
              " finished!")
    return output_analytics


def print_output_analytics_list(output_analytics_list, sub_output_analytics, interval):
    now = datetime.datetime.now()
    time_str = now.strftime("%Y%m%dT%H%M%S")
    optimal_parameters_path_csv = os.path.join(output_directory, str(interval) + "_optimal_parameters_" + time_str + ".csv")
    optimal_parameters_path_txt = os.path.join(output_directory, str(interval) + "_optimal_parameters_" + time_str + ".txt")
    # write sub_output_analytics
    f = open(optimal_parameters_path_txt, 'w')
    for line in sub_output_analytics:
        f.write(line + "\n")
    f.close()
    # write output_analytics_list
    with open(optimal_parameters_path_csv, 'wb') as csv_file:
        fieldnames = ['Random seed', 'Number of trees in random forest', 'Min samples per leaf in random forest',
                      'Max features per tree', 'Max depth for random forest', 'Auc', 'Threshold', 'Precision', 'Recall',
                      'Precision & Recall avg', '0.6*Precision + 0.4*Recall', '0.4*Precision + 0.6*Recall']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for output_analytics in output_analytics_list:
            precision = float(str(output_analytics[7]).split(':')[1])
            recall = float(str(output_analytics[8]).split(':')[1])
            writer.writerow({'Random seed': str(output_analytics[4]).split(':')[1],
                             'Number of trees in random forest': str(output_analytics[0]).split(':')[1],
                             'Min samples per leaf in random forest': str(output_analytics[1]).split(':')[1],
                             'Max features per tree': str(output_analytics[2]).split(':')[1],
                             'Max depth for random forest': str(output_analytics[3]).split(':')[1],
                             'Auc': str(output_analytics[5]).split(':')[1],
                             'Threshold': str(output_analytics[6]).split(':')[1],
                             'Precision': str(precision),
                             'Recall': str(recall),
                             'Precision & Recall avg': str((precision+recall)/2),
                             '0.6*Precision + 0.4*Recall': str((0.6*precision+0.4*recall)),
                             '0.4*Precision + 0.6*Recall': str((0.4*precision+0.6*recall))
                             })
        csv_file.close()


def process_classifier(classifier, classifier_name, train_x, train_y, test_x, test_y, customers_ids, interval,
                       sub_output_analytics, thresholds, max_depth, n_estimators, min_samples_leaf, max_features,
                       check_params_effectiveness=False, show_graph=False,
                       random_seed=None):

    myLog.log("Fit for " + classifier_name)
    ##################################
    # fit                            #
    ##################################
    classifier.fit(train_x, train_y)
    ##################################
    # serialize the classifiers      #
    ##################################
    now = datetime.datetime.now()
    time_str = now.strftime("%Y%m%dT%H%M%S")
    file_name = str(interval) + "_" + classifier_name + "_" + time_str + ".p"
    pickle.dump(classifier, open(os.path.join(classifiers_directory, file_name), "wb"))
    myLog.log("calculate prediction for interval = " + str(interval) + " for " + classifier_name)
    predict = classifier.predict_proba(test_x)
    output_prediction("churn", predict, customers_ids, interval, time_str)
    output_analytics = calculate_analytics(classifier, classifier_name, predict, test_x, test_y, interval,
                                           sub_output_analytics, thresholds,
                                           time_str, max_depth, n_estimators, min_samples_leaf, max_features,
                                           check_params_effectiveness, show_graph, random_seed)
    # print output_analytics
    output_analytics_to_file(classifier_name, output_analytics, str(interval), time_str)
    return file_name


def calculate_analytics(classifier, classifier_name, predict, test_x, test_y, interval, sub_output_analytics, thresholds,
                        time_str, max_depth, n_estimators, min_samples_leaf, max_features,
                        check_params_effectiveness=False, show_graph=False, random_seed=None):
    ##################################
    # output analytics               #
    ##################################
    output_analytics = []
    for output in sub_output_analytics:
        output_analytics.append(output)
    # n_estimators
    output_analytics.append('Number of trees in random forest is: ' + str(n_estimators))
    # min_samples_leaf
    output_analytics.append('Min samples per leaf in random forest is: ' + str(min_samples_leaf))
    # max_features
    output_analytics.append('Max features per tree in random forest is: ' + str(max_features))
    # max_depth
    output_analytics.append('Max depth for random forest is: ' + str(max_depth))
    # random_seed for random forest
    if random_seed is not None:
        output_analytics.append('Random seed for random forest: ' + str(random_seed))
    # auc
    roc_auc = metrics.roc_auc_score(test_y, predict[:, 1])
    output_analytics.append('the ' + classifier_name + ' auc: ' + str(roc_auc))
    # thresholds
    if isinstance(thresholds, float):
        thresholds = [thresholds]
    for threshold in thresholds:
        ##################################
        # Precision & Recall             #
        ##################################
        output_analytics.append('Threshold :' + str(threshold))
        precision, recall = precision_and_recall(predict=predict[:, 1], actual=test_y, threshold=threshold)
        output_analytics.append('Precision for threshold ' + str(threshold) + ': ' + str(precision))
        output_analytics.append('Recall for threshold ' + str(threshold) + ': ' + str(recall))
    ##################################
    # params_effectiveness & graph   #
    ##################################
    if check_params_effectiveness:
        output_param_effectiveness_file(classifier, classifier_name, test_x, interval, time_str)
    if show_graph:
        save_roc_graph(classifier_name, test_y, predict, roc_auc, interval, time_str)
    return output_analytics


###############################################################
# paths                                                       #
###############################################################
src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr) - 1])
resources_directory = os.path.join(project_directory, "resources")
parameters_file = os.path.join(resources_directory, "parameters.txt")
log_file = os.path.join(os.path.join(project_directory, "logs"), "log.txt")
tmp_directory = os.path.join(project_directory, "tmp")
data_directory = os.path.join(project_directory, "data")
output_directory = os.path.join(project_directory, "output")
classifiers_directory = os.path.join(output_directory, "classifiers")

###############################################################
# parameters                                                  #
###############################################################
paths_of_files_to_remove = []
label_col = 0
customers_col = 1
min_data_col = 2

###############################################################
# main                                                        #
###############################################################

# def create_data(interval, market_directory="", cosmos_directory="", train_all_files_in_list=False):
#     all_files_path = os.path.join(resources_directory, str(interval) + "_all_files.txt")
#     if train_all_files_in_list:
#         # training data is compose from all files
#         # there is an overlap with the test file!
#         data_train_list, data_tst_list, sub_output_analytics = get_list_of_files(all_files_path,
#                                                                                  train_all_files_in_list,
#                                                                                  interval, 0, 1,
#                                                                                  market_directory, cosmos_directory)
#     else:
#         # training data is compose from all files but one
#         # there is no overlap!
#         data_train_list, data_tst_list, sub_output_analytics = get_list_of_files(all_files_path,
#                                                                                  train_all_files_in_list,
#                                                                                  interval, 1, 1,
#                                                                                  market_directory, cosmos_directory)
#     union_data_path = os.path.join(tmp_directory, str(interval) + "_union")
#     union_test_path = os.path.join(tmp_directory, str(interval) + "_tst_union.csv")
#     data_train = data_per_files(data_train_list, True, union_data_path)
#     data_tst = data_per_files(data_tst_list, True, union_test_path)
#     return data_train, data_tst, sub_output_analytics

# def data_per_files(list_files, additional_file, additional_file_path):
#     # this would be the index in teh list of files of the first file. we need it because in some cases
#     #  the first row in the file that contains the list of files is empty
#     first_file_inx = 0
#     if str(list_files[first_file_inx]).__len__() < 2:
#         first_file_inx = 1
#     while '#' in list_files[first_file_inx]:
#         myLog.log('skipping: ' + list_files[first_file_inx])
#         first_file_inx += 1
#
#     myLog.log(list_files[first_file_inx])
#     data_2 = get_data_from_path(list_files[first_file_inx], additional_file, additional_file_path)
#     print 'amount of files: ' + str(len(list_files))
#     for i in range(first_file_inx + 1, (len(list_files))):
#         if '#' not in list_files[i]:
#             myLog.log(list_files[i])
#             new_data = get_data_from_path(list_files[i], additional_file, additional_file_path)
#             data_2 = np.concatenate((data_2, new_data), axis=0)
#     paths_of_files_to_remove.append(additional_file_path)
#     return data_2


# def get_data_from_path(a_file_path, additional_file, additional_file_path):
#     # prepare_file(a_file_path)
#     f = file(a_file_path, "r")
#     first_row = f.readline()
#     if first_row.__len__() < 5:
#         first_row = f.readline()
#     if first_row.split(',')[0].strip().isdigit():
#         # fix bug that the first line was not re-read
#         f = file(a_file_path, "r")
#         data_1 = np.loadtxt(f, delimiter=',')
#     if additional_file:
#         for row in data_1:
#             file(u"%s" % additional_file_path, "a").write(",".join([str(i) for i in row]) + "\n")
#     return data_1
