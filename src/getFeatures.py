import os
from os import remove, close
from tempfile import mkstemp
from shutil import move

from datetime import timedelta, date

from redshift import *


def get_feature_data(intervals, table_name, directory_name, first_date, last_date, customers=None):
    h_users_buy_sell_query = """DROP TABLE IF EXISTS h_users_buy_sell;
                                CREATE TABLE h_users_buy_sell AS
                                SELECT
                                       -- customer id
                                       onlyBuySell.customer_id customer_id,
                                       -- #days between first and last time a position was opened
                                       datediff(day, MIN(open_time), MAX(open_time)) cnt_days_opend_positions,
                                       -- #open positions at the reference date
                                       SUM(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 1 ELSE 0 END) sum_open_pos,
                                       -- sum_open_pos divided by cnt_positions
                                      SUM(CASE WHEN (close_time = '1970-01-01' OR close_time > a_date) AND cmd != 6 THEN 1.0 ELSE 0.0 END)/count(1) share_open_pos,
                                      -- similar to share_open_pos, only each position get a weight by its volume
                                      SUM((CASE WHEN (close_time = '1970-01-01' OR close_time > a_date) AND cmd != 6 THEN 1.0 ELSE 0.0 END)*volume)/greatest(1,sum(volume)) share_volume_open_pos,
                                       -- share buysell
                                       SUM(CASE WHEN buysell = '1' THEN 1.0 ELSE 0.0 END)/COUNT(1) share_buysell,
                                       SUM(CASE WHEN symbol LIKE 'EURUSD' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'EURUSD' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_EURUSD,
                                       SUM(CASE WHEN symbol LIKE 'Oil' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'Oil' then 1.0 else 0.0 end)+ 1) share_buysell_Oil,
                                       SUM(CASE WHEN symbol LIKE 'USA30' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'USA30' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_USA30,
                                       SUM(CASE WHEN symbol LIKE 'GBPUSD' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'GBPUSD' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_GBPUSD,
                                       SUM(CASE WHEN symbol LIKE 'XAUUSD' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'XAUUSD' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_XAUUSD,
                                       SUM(CASE WHEN symbol LIKE 'USDJPY' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'USDJPY' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_USDJPY,
                                       SUM(CASE WHEN symbol LIKE 'USDCAD' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'USDCAD' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_USDCAD,
                                       SUM(CASE WHEN symbol LIKE 'USDCHF' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'USDCHF' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_USDCHF,
                                       SUM(CASE WHEN symbol LIKE 'USA500' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'USA500' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_USA500,
                                       SUM(CASE WHEN symbol LIKE 'AUDUSD' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'AUDUSD' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_AUDUSD,
                                       SUM(CASE WHEN symbol LIKE 'GBPJPY' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'GBPJPY' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_GBPJPY,
                                       SUM(CASE WHEN symbol LIKE 'EURJPY' AND buysell = '1' THEN 1.0 ELSE 0.0 END)/(SUM(CASE WHEN symbol LIKE 'EURJPY' THEN 1.0 ELSE 0.0 END) + 1) share_buysell_EURJPY,
                                       -- share volume & profit buysell
                                       SUM((CASE WHEN buysell = '1' THEN 1.0 ELSE 0.0 END)*volume)/(CASE WHEN SUM(volume) = 0 THEN 1.0 ELSE SUM(volume) END) share_volume_buysell,
                                       SUM(ABS((CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*profitusd*(CASE WHEN buysell = '1' THEN 1.0 ELSE 0.0 END)))/(SUM(ABS((CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*profitusd)) + 1) share_profit_buysell,
                                       -- sum of volume of all positions
                                       SUM(volume) sum_volume,
                                       -- sum positions (with decay weight for each position)
                                       SUM(POWER(0.5, datediff(second, open_time, a_date)/3600)) sum_positions_hour,
                                       SUM(POWER(0.5, datediff(hour, open_time, a_date)/24)) sum_positions_day,
                                       SUM(POWER(0.5, datediff(day, open_time, a_date)/7)) sum_positions_week,
                                       SUM(POWER(0.5, datediff(day, open_time, a_date)/30)) sum_positions_month,
                                       -- sum volume positions - weight of each position is multiplied by its volume (with decay weight for each position)
                                       SUM(volume*POWER(0.5, datediff(second, open_time, a_date)/3600)) sum_volume_positions_hour,
                                       SUM(volume*POWER(0.5, datediff(hour, open_time, a_date)/24)) sum_volume_positions_day,
                                       SUM(volume*POWER(0.5, datediff(day, open_time, a_date)/7)) sum_volume_positions_week,
                                       SUM(volume*POWER(0.5, datediff(day, open_time, a_date)/30)) sum_volume_positions_month,
                                       -- share sum volume positions - weight of each position is multiplied by its volume (with decay weight for each position)
                                       SUM(volume*POWER(0.5, datediff(second, open_time, a_date)/3600))/(CASE WHEN SUM(volume) = 0 THEN 1.0 ELSE SUM(volume) END) sum_volume_positions_hour_by_sum_volume,
                                       SUM(volume*POWER(0.5, datediff(hour, open_time, a_date)/24))/(CASE WHEN SUM(volume) = 0 THEN 1.0 ELSE SUM(volume) END) sum_volume_positions_day_by_sum_volume,
                                       SUM(volume*POWER(0.5, datediff(day, open_time, a_date)/7))/(CASE WHEN SUM(volume) = 0 THEN 1.0 ELSE SUM(volume) END) sum_volume_positions_week_by_sum_volume,
                                       SUM(volume*POWER(0.5, datediff(day, open_time, a_date)/30))/(CASE WHEN SUM(volume) = 0 THEN 1.0 ELSE SUM(volume) END) sum_volume_positions_month_by_sum_volume,
                                       -- share symbols
                                       SUM(CASE WHEN symbol LIKE 'EURUSD' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_EURUSD,
                                       SUM(CASE WHEN symbol LIKE 'Oil' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_Oil,
                                       SUM(CASE WHEN symbol LIKE 'USA30' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_USA30,
                                       SUM(CASE WHEN symbol LIKE 'GBPUSD' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_GBPUSD,
                                       SUM(CASE WHEN symbol LIKE 'XAUUSD' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_XAUUSD,
                                       SUM(CASE WHEN symbol LIKE 'USDJPY' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_USDJPY,
                                       SUM(CASE WHEN symbol LIKE 'USDCAD' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_USDCAD,
                                       SUM(CASE WHEN symbol LIKE 'USDCHF' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_USDCHF,
                                       SUM(CASE WHEN symbol LIKE 'USA500' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_USA500,
                                       SUM(CASE WHEN symbol LIKE 'AUDUSD' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_AUDUSD,
                                       SUM(CASE WHEN symbol LIKE 'GBPJPY' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_GBPJPY,
                                       SUM(CASE WHEN symbol LIKE 'EURJPY' THEN 1.0 ELSE 0.0 END)/(COUNT(1) + 1) share_EURJPY,
                                       -- share profit (usd)
                                       SUM((CASE WHEN symbol LIKE 'EURUSD' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_EURUSD,
                                       SUM((CASE WHEN symbol LIKE 'Oil' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_Oil,
                                       SUM((CASE WHEN symbol LIKE 'USA30' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_USA30,
                                       SUM((CASE WHEN symbol LIKE 'GBPUSD' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_GBPUSD,
                                       SUM((CASE WHEN symbol LIKE 'XAUUSD' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_XAUUSD,
                                       SUM((CASE WHEN symbol LIKE 'USDJPY' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_USDJPY,
                                       SUM((CASE WHEN symbol LIKE 'USDCAD' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_USDCAD,
                                       SUM((CASE WHEN symbol LIKE 'USDCHF' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_USDCHF,
                                       SUM((CASE WHEN symbol LIKE 'USA500' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_USA500,
                                       SUM((CASE WHEN symbol LIKE 'AUDUSD' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_AUDUSD,
                                       SUM((CASE WHEN symbol LIKE 'GBPJPY' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_GBPJPY,
                                       SUM((CASE WHEN symbol LIKE 'EURJPY' THEN 1.0 ELSE 0.0 END)*profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) share_profitusd_EURJPY,
                                       -- sum of profit (usd) made so far
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) sum_profit,
                                       -- same as sum_profit, only each profit get a decay weight
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(second, open_time, a_date)/3600)) sum_profitusd_positions_hour,
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(hour, open_time, a_date)/24)) sum_profitusd_positions_day,
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/7)) sum_profitusd_positions_week,
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/30)) sum_profitusd_positions_month,
                                       -- sum profit with decay constant of {interval} divided by the total sum of profit
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(second, open_time, a_date)/3600))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) sum_profitusd_positions_hour_by_sum_profitusd,
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(hour, open_time, a_date)/24))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) sum_profitusd_positions_day_by_sum_profitusd,
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/7))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) sum_profitusd_positions_week_by_sum_profitusd,
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/30))/(CASE WHEN SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) sum_profitusd_positions_month_by_sum_profitusd,
                                       -- same as sum_pforit, only each profit is with a positive sign. This gives us a sense of the amount of volume transfer between the user and the system
                                       SUM(ABS(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))) sum_abs_profit,
                                       SUM(ABS(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))*POWER(0.5, datediff(day, open_time, a_date)/7)) sum_abs_profit_week,
                                       SUM(ABS(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END))*POWER(0.5, datediff(day, open_time, a_date)/30)) sum_abs_profit_month,
                                       -- expected_abs_profit_in_absent
                                       sum(abs(profitusd*(case when close_time = '1970-01-01' or close_time > a_date then 0.0 else 1.0 end)))*min(datediff(day, open_time, a_date))*1.0/greatest(1,datediff(day, min(open_time), max(open_time))) expected_abs_profit_in_absent,
                                       -- total sum of profit with decay constant of {interval}, divided by sum_abs_profit_{interval}
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/7))/(CASE WHEN SUM(ABS(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/7))) = 0 THEN 1.0 ELSE SUM(ABS(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/7))) END ) sum_profit_by_abs_profit_week,
                                       SUM(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/30))/(CASE WHEN SUM(ABS(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/30))) = 0 THEN 1.0 ELSE SUM(ABS(profitusd*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/30))) END ) sum_profit_by_abs_profit_month,
                                       -- similar to sum_profitusd_positions_hour_by_sum_profitusd, only with abs(profitusd), instead of profitusd
                                       SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(second, open_time, a_date)/3600))/(CASE WHEN SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) sum_abs_profitusd_positions_hour_by_sum_abs_profitusd,
                                       SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(hour, open_time, a_date)/24))/(CASE WHEN SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) sum_abs_profitusd_positions_day_by_sum_abs_profitusd,
                                       SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/7))/(CASE WHEN SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) sum_abs_profitusd_positions_week_by_sum_abs_profitusd,
                                       SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*POWER(0.5, datediff(day, open_time, a_date)/30))/(CASE WHEN SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) = 0 THEN 1.0 ELSE SUM(ABS(profitusd)*(CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)) END) sum_abs_profitusd_positions_month_by_sum_abs_profitusd,
                                       -- total time of difference between the opening time of each pos and the closing time
                                       SUM((CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*datediff(second, open_time, GREATEST(open_time, close_time))) toatl_time_sec,
                                       -- same as toatl_time_sec, only each position time gets a decay weight, by 0.5 per month
                                       SUM((CASE WHEN close_time = '1970-01-01' OR close_time > a_date THEN 0.0 ELSE 1.0 END)*datediff(second, open_time, GREATEST(open_time, close_time))*POWER(0.5, datediff(day, open_time, a_date)/30)) toatl_time_sec_dcay_month

                                FROM (SELECT *
                                      FROM %(table_name)s
                                      WHERE cmd != 6) onlyBuySell
                                JOIN tmp_vars ON true
                                JOIN h_last_month_users lastMonthUsers ON onlyBuySell.customer_id = lastMonthUsers.customer_id
                                WHERE open_time < a_date
                                GROUP BY onlyBuySell.customer_id
                                ; """ % {"table_name": table_name}

    h_recency_query = """DROP TABLE IF EXISTS h_recency;
                    CREATE TABLE h_recency AS
                    SELECT t1.customer_id,
                           t1.recency_open,
                           t1.recency_close,
                           t2.recency_withdrawal,
                           t2.recency_deposit,
                           t2.recency_bonus,
                           t2.recency_adjustment
                    FROM (SELECT
                                  customer_id,
                                  -- #days since last time the user opened a position
                                  MIN(datediff(day, open_time, a_date)) recency_open,
                                  -- recency: #days since last time the user closed a position
                                  MIN(datediff(day, close_time, a_date)) recency_close
                          FROM %(table_name)s
                          JOIN tmp_vars ON true
                          WHERE open_time < a_date AND close_time < a_date AND cmd != 6
                          GROUP BY customer_id) t1
                    JOIN (SELECT
                                customer_id,
                                MIN(CASE WHEN trans_type = 'withdrawal' THEN datediff(day, time_stamp, a_date) ELSE -999 END) recency_withdrawal,
                                MIN(CASE WHEN trans_type = 'deposit' THEN datediff(day, time_stamp, a_date) ELSE -999 END) recency_deposit,
                                MIN(CASE WHEN trans_type = 'bonus' THEN datediff(day, time_stamp, a_date) ELSE -999 END) recency_bonus,
                                MIN(CASE WHEN trans_type = 'correction' THEN datediff(day, time_stamp, a_date) ELSE -999 END) recency_adjustment
                          FROM y_offline_customers_transactions
                          JOIN tmp_vars ON true
                          WHERE time_stamp < a_date
                          GROUP BY customer_id) t2
                    ON t1.customer_id = t2.customer_id
                    ;""" % {"table_name": table_name}

    h_users_general = """DROP TABLE IF EXISTS h_users_general;
                        CREATE TABLE h_users_general AS
                        SELECT
                              customer_id,
                              -- total amount of positions that were opened by/for the user.
                              COUNT(*) cnt_positions,
                              -- sum profit (usd)
                              sum(profitusd) sum_profitusd,
                              -- #symbols
                              COUNT(DISTINCT symbol) cnt_symbols,
                              -- #accounts
                              COUNT(DISTINCT account_id) cnt_accounts,
                              -- #platforms
                              COUNT(DISTINCT platform) cnt_platforms

                        FROM %(table_name)s
                        JOIN tmp_vars ON true
                        WHERE open_time < a_date
                        GROUP BY customer_id
                        ;""" % {"table_name": table_name}

    for interval in intervals:
        query_interval_period = """-- take only users from last month
                                DROP TABLE IF EXISTS h_last_month_users;
                                CREATE TABLE h_last_month_users AS
                                SELECT DISTINCT customer_id
                                  FROM  %(table_name)s
                                  JOIN tmp_vars ON true
                                  WHERE open_time < a_date AND open_time >  dateadd(day, -%(interval)s, a_date)
                                ;

                                DROP TABLE IF EXISTS h_future_users;
                                CREATE TABLE h_future_users AS
                                SELECT
                                    customer_id,
                                    0 churn
                                FROM %(table_name)s
                                JOIN tmp_vars ON open_time > a_date AND open_time < dateadd(day, %(interval)s, a_date)
                                GROUP BY customer_id
                                ;""" % {"interval": interval, "table_name": table_name}

        with open(query_path) as myFile:
            query_string = "".join(line for line in myFile)

        myLog.log("create data for interval = " + str(interval))
        final_dates = create_dates(interval, first_date, last_date)

        for date in final_dates:
            query_date_string = """-- reference date table
                            DROP TABLE IF EXISTS tmp_vars;
                            CREATE temp TABLE tmp_vars (
                              a_date DATE
                            );
                            INSERT INTO tmp_vars (a_date) VALUES
                              ('%(date)s')
                             ;\n""" % {"date": date}

            if customers is None:
                customers_query = ""
            else:
                customers_query = " WHERE "
                for i in range(len(customers)):
                    customers_query += " customer_id = '%(id)s' " % {"id": str(customers[i].customer_id)}
                    if i != len(customers) - 1:
                        customers_query += " OR "

            final_query = query_date_string + \
                          query_interval_period + \
                          h_users_buy_sell_query + \
                          h_recency_query + \
                          h_users_general + \
                          query_string + \
                          " SELECT * FROM h_train_churn %(customers_query)s ;" % {"customers_query": customers_query}

            if customers is None:
                file_name = "%(name)s.txt" % {"name": interval + "_" + date.replace("-", "")}
                text_file_path = os.path.join(os.path.join(home_directory_data, directory_name), file_name)
                # check if file exist already
                file_exist = os.path.isfile(text_file_path)
                if not file_exist:
                    # write the data to tmp file
                    rows = query_redshift(final_query)
                    print "Writing file: " + file_name + " to " + directory_name + " directory"
                    write_rows_to_file(text_file_path, rows)
                else:
                    print "File: " + file_name + " from " + directory_name + " directory already exist"
            else:
                rows = query_redshift(final_query)
                return rows


def write_rows_to_file(text_file_path, rows):
    text_file = open(text_file_path, "w")
    for row in rows:
        for i in range(len(row)):
            text_file.write(str(row[i]))
            if i != len(row) - 1:
                text_file.write(",")
        text_file.write("\n")
    text_file.close()


def get_features_names(table_name):
    cols = get_columns_names(table_name)
    text_file = open(os.path.join(resources_directory, "parameters.txt"), "w")
    for i in range(len(cols)):
        if i > 1:
            text_file.write(cols[i])
            if i != len(cols) - 1:
                text_file.write("\n")
    text_file.close()


def get_all_columns_names(table_name):
    cols = get_columns_names(table_name)
    text_file = open(os.path.join(resources_directory, "parameters.txt"), "w")
    for col in cols:
        text_file.write(col)
        text_file.write("\n")
    text_file.close()


def convert2datetime(last_date):
    last_date_arr = last_date.split("-")
    return date(int(last_date_arr[0]), int(last_date_arr[1]), int(last_date_arr[2]))


# def convert2int(day):
#     if day[0] == '0':
#         return int(day[1])
#     return int(day)


# def check_relevant_day(interval, day, last_date_day):
#     last_date_day_int = convert2int(last_date_day)
#     if (convert2int(day) + int(interval)) <= last_date_day_int:
#         return True
#     return False


def create_dates(interval, first_date, last_date):
    interval_delta = timedelta(int(interval))
    last_date_format = convert2datetime(last_date)
    first_date_format = convert2datetime(first_date)
    final_dates = []
    current_date = last_date_format - interval_delta
    print current_date
    final_dates.append(current_date.isoformat())
    while current_date - interval_delta >= first_date_format:
        current_date = current_date - interval_delta
        final_dates.append(current_date.isoformat())
        print current_date
    return final_dates[::-1]
    # last_date_year, last_date_month, last_date_day = last_date_split(last_date)
    # final_dates = []
    # if interval == "5":
    #     days = ["01", "06", "11", "16", "21", "26"]
    # elif interval == "7":
    #     days = ["01", "08", "15", "22"]
    # elif interval == "14":
    #     days = ["01", "15"]
    # elif interval == "30":
    #     days = ["01"]
    #
    # for date in dates:
    #     date_arr = date.split("-")
    #     date_year = date_arr[0]
    #     date_month = date_arr[1]
    #     if date_month != last_date_month or date_year != last_date_year:
    #         for day in days:
    #             final_dates.append(date + "-" + day)
    #     else:
    #         for day in days:
    #             if check_relevant_day(interval, day, last_date_day):
    #                 final_dates.append(date + "-" + day)
    #
    # return final_dates


def add_sharps_to_files(all_files_path):
    file_path = all_files_path
    # Create temp file
    fh, abs_path = mkstemp()
    with open(abs_path, 'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write("#" + line)
    close(fh)
    # Remove original file
    remove(file_path)
    # Move new file
    move(abs_path, file_path)


def rewrite_all_files(interval, first_date, last_date):
    all_files_path = os.path.join(resources_directory, interval + "_all_files.txt")
    all_files = []
    my_files = create_dates(interval, first_date, last_date)
    for myFile in my_files:
        all_files.append(interval + "_" + str(myFile).replace("-", "") + ".txt")
    file_exist = os.path.isfile(all_files_path)
    if file_exist:
        myLog.log("Rewrite all_files.txt")
        add_sharps_to_files(all_files_path)
        with open(all_files_path, "a") as text_file:
            for file_name in all_files:
                text_file.write(file_name + "\n")
    else:
        myLog.log("Write all_files.txt for the first time")
        text_file = open(all_files_path, "w")
        for file_name in all_files:
            text_file.write(file_name + "\n")
    text_file.close()

##################################################################################

src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr) - 1])
home_directory_data = os.path.join(project_directory, "data")
resources_directory = os.path.join(project_directory, "resources")
query_path = os.path.join(resources_directory, "query.txt")

# getFeatureData = True
# getFeatureName = False
# if getFeatureData:
#     get_feature_data()
# if getFeatureName:
#     get_feature_name("h_train_churn")
