import os
from multiprocessing import Process
import myLog
from CLV_Regressor import calculate_clv
from getClassifiers import calculate_classifiers, load_classifier, find_optimal_parameters
from getFeatures import get_feature_data, get_features_names, rewrite_all_files

##################################################

"""
Notes:
1. There are hard-coded constrains on getFeatures.create_dates function.
   The reason for that is to make the data complete and precisely according to the interval.
2. If the training data compose from market + cosmos then there will be 2 test files for the last file.
3. train_all_files_in_list - without testing. For online classification.

"""

##################################################

src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr) - 1])
home_directory_output = os.path.join(project_directory, "output")
classifiers_directory = os.path.join(home_directory_output, "classifiers")
classifiers_names = []


def create_and_load_classifier(p_dates, p_interval, p_thresholds, p_n_estimators, p_max_features, p_min_samples_leaf,
                               p_max_depth, p_check_params_effectiveness, p_show_graph,
                               p_train_all_files_in_list, p_last_date, p_market_directory="", p_cosmos_directory=""):

    rewrite_all_files(p_dates, str(p_interval), p_last_date)
    myLog.log("calculate classifiers for interval of " + str(p_interval) + " days")
    classifier_name = calculate_classifiers(p_interval, p_thresholds, p_n_estimators, p_max_features,
                                            p_min_samples_leaf, p_max_depth, p_check_params_effectiveness, p_show_graph,
                                            p_train_all_files_in_list, p_market_directory, p_cosmos_directory)
    return classifier_name


##################################################
# PARAMETERS                                     #
##################################################

# Modes parameters
##################################################
# Mode 1
crate_classifiers = True
# Mode 2
load_old_classifiers = False
# Mode 3
optimal_parameters = False
# CLV
clv = False

# Rewrite parameters
##################################################
recreate_features_names = False
create_data = True
last_date = "2016-05-22"
cosmos_table = " y_offline_positions_from_cosmos  "
market_table = ""
# market_table = " y_offline_positions_from_source  "
# market_directory = "y_market"
market_directory = ""
cosmos_directory = "y_cosmos"
churn_table = "h_train_churn"

# General parameters
##################################################
# intervals_days = ["7", "14", "30", "5"]
# dates = ["2015-05", "2015-06", "2015-07", "2015-08", "2015-09", "2015-10", "2015-11", "2015-12", "2016-01", "2016-02",
#          "2016-03", "2016-04", "2016-05"]
intervals_days = ["30"]
# dates = ["2015-05", "2015-06"]
# dates = ["2015-02", "2015-03"]
dates = ["2015-02", "2015-03", "2015-04", "2015-05", "2015-06", "2015-07",
         "2015-08", "2015-09", "2015-10", "2015-11", "2015-12", "2016-01",
         "2016-02", "2016-03", "2016-04", "2016-05"]

# Mode 1 parameters
##################################################
thresholds = [0.5, 0.6, 0.7]
train_all_files_in_list = False
check_params_effectiveness = False
show_graph = False
n_estimators = 30
max_features = 35
min_samples_leaf = 10
max_depth = 20

# Mode 2 parameters
##################################################
# threshold = 0.7
classifier_path = "C:\\Users\\hairo\\Documents\\MarketChurnAutomatic\\output\\classifiers\\" \
                  "30_clf_random_forest_20160509T145636.p"
data_tst_list = ["C:\\Users\\hairo\\Documents\\MarketChurnAutomatic\\data\\y_cosmos\\30_20160301.txt"]


# Mode 3 parameters
##################################################
# check_max_depth = None
# check_n_estimators = None
# check_min_sample_leaf = None
# check_max_features = None
check_max_depth = [20, None]
check_n_estimators = [20, 30]
check_min_sample_leaf = [5, 10]
check_max_features = [35]


####################################################################################################

# TODO last_reference_date = "2016-04-20"

if __name__ == '__main__':

    if create_data:
        if market_table is not "" and market_directory is not "":
            myLog.log("calculate data for market")
            get_feature_data(intervals_days, dates, market_table, market_directory, last_date)
        if cosmos_table is not "" and cosmos_directory is not "":
            myLog.log("calculate data for cosmos")
            get_feature_data(intervals_days, dates, cosmos_table, cosmos_directory, last_date)

    if recreate_features_names:
        myLog.log("calculate features names")
        get_features_names(churn_table)

    # create new classifier
    if crate_classifiers or load_old_classifiers:
        processes = []
        for interval in intervals_days:
            if crate_classifiers:
                process = Process(target=create_and_load_classifier,
                                  args=(dates, interval, thresholds, n_estimators, max_features, min_samples_leaf,
                                        max_depth, check_params_effectiveness, show_graph, train_all_files_in_list,
                                        last_date, market_directory, cosmos_directory,))
                process.start()
                processes.append(process)
            if load_old_classifiers:
                process = Process(target=load_classifier,
                                  args=(classifier_path, interval, data_tst_list, thresholds,))
                process.start()
                processes.append(process)
        for process in processes:
            process.join()

    if optimal_parameters:
        processes = []
        for interval in intervals_days:
            rewrite_all_files(dates, str(interval), last_date)
            process = Process(target=find_optimal_parameters,
                              args=(interval, market_directory, cosmos_directory, thresholds, check_max_depth,
                                    check_n_estimators, check_min_sample_leaf, check_max_features,))
            process.start()
            processes.append(process)
        for process in processes:
            process.join()

    if clv:
        calculate_clv(market_directory, cosmos_directory)
    myLog.log("--------------------------------------------------------")
