import os
from time import strftime, gmtime

src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr) - 1])
log_file = os.path.join(os.path.join(project_directory, "logs"), "log.txt")


def log(statement, obj_type='str'):
    str_out = ''
    if obj_type == 'dictionary':
        for key in statement:
            str_out += '\n' + str(key) + ': ' + str(statement[key])
    if obj_type == 'str':
        str_out = "\n" + strftime("%Y-%m-%d %H:%M:%S", gmtime()) + ": " + statement
    if obj_type == 'array':
        str_out = ''
        for a in statement:
            str_out += str(a)
    file(u"%s" % log_file, "a").write(str_out)
    print str_out

