import os

import datetime
import numpy as np
import xlsxwriter

from redshift import query_redshift


def combination_list(l1, l2, combination):
    l_combined = []
    for i in range(len(l1)):
        l_combined.append(combination_nums(l1[i], l2[i], combination))
    return l_combined


def combination_nums(x, y, combination):
    if 'max' == combination:
        return max(x, y)
    if 'min' == combination:
        return min(x, y)
    if 'avg' == combination:
        return 0.5*(x+y)
    return x


def combination_nums3(x, y, z, combination):
    if 'max' == combination:
        return max(x, y, z)
    if 'min' == combination:
        return min(x, y, z)
    if 'avg' == combination:
        return (x+y+z)/3.0
    return x


def combination_list3(l1, l2, l3, combination):
    l_combined = []
    for i in range(len(l1)):
        l_combined.append(combination_nums3(l1[i], l2[i], l3[i], combination))
    return l_combined


def print_rows(rows):
    print "Show me the databases:"
    for row in rows:
        print ""
        for i in range(len(row)):
            print row[i],
    print ""


def convert2features(rows, data_path, to_print=False):
    # write the data to tmp file
    text_file = open(data_path, "w")
    for row in rows:
        for i in range(len(row)):
            text_file.write(str(row[i]))
            if i != len(row) - 1:
                text_file.write(",")
        text_file.write("\n")
    text_file.close()
    if to_print:
        print_rows(rows)
        print "------------------------------------"

    # loads the data using numpy
    test_file = file(data_path, "r")
    data = np.loadtxt(test_file, delimiter=',')
    test_file.close()
    if len(rows) > 1:
        test_X = data[:, 2:]
        customers_ids = data[:, 1]
    elif len(rows) == 1:
        test_X = [data[2:]]
        customers_ids = [data[1]]
    # len(rows) < 1
    else:
        return None, None
    return test_X, customers_ids


##########################################################################

# all paths
src_directory = os.getcwd()
project_directory_arr = os.getcwd().split("\\")
project_directory = "\\".join(project_directory_arr[:len(project_directory_arr)-1])
home_directory_data = os.path.join(project_directory, "data")
data_path = os.path.join(home_directory_data, "test.txt")
home_directory_output = os.path.join(project_directory, "output")
output_prediction_directory = os.path.join(project_directory, "output_prediction")
classifiers_directory = os.path.join(home_directory_output, "classifiers")
# clf_random_forest_directory = os.path.join(classifiers_directory, "save_clf_random_forest.p")
# clf_random_forest = pickle.load(open(clf_random_forest_directory, "rb"))
# tables names

##########################################################################


def predict(query_string, clf_random_forest):
    rows = query_redshift(query_string)
    train_x, customers_ids = convert2features(rows, data_path)
    # remove tmp file if exist
    if os.path.isfile(data_path):
        os.remove(data_path)
    if train_x is not None and customers_ids is not None:
        predict_random_forest = clf_random_forest.predict_proba(train_x)
        return to_json(predict_random_forest, customers_ids)
    else:
        return None


def to_json(predict_random_forest, customers_ids):
    output = []
    for i in range(len(predict_random_forest)):
        # prediction: [won't_churn_prob , churn_prob] , [won't_churn_prob , churn_prob]
        data = {'customerId': int(customers_ids[i]), 'churnProbability': predict_random_forest[i][1]}
        output.append(data)
    return output


def predict_all_customers(clf_random_forest, churn_table):
    # get data from SQL query
    query_string = "SELECT * FROM %(churn_table)s;" % {"churn_table": churn_table}
    output = predict(query_string, clf_random_forest)
    return output


def predict_all_customers_prob(prob, direction):
    final_output = []
    probability = float(prob)
    output = predict_all_customers()
    if direction == 'greater':
        for result in output:
            if float(result['churnProbability']) > probability:
                final_output.append(result)
    elif direction == 'smaller':
        for result in output:
            if float(result['churnProbability']) < probability:
                final_output.append(result)
    elif direction == 'equal':
        for result in output:
            if float(result['churnProbability']) == probability:
                final_output.append(result)
    elif direction == 'greaterOrEqual':
        for result in output:
            if float(result['churnProbability']) >= probability:
                final_output.append(result)
    elif direction == 'lessOrEqual':
        for result in output:
            if float(result['churnProbability']) <= probability:
                final_output.append(result)
    return final_output


def predict_specific_customers(customers, clf_random_forest, churn_table):
    query_string = "SELECT * FROM %(churn_table)s WHERE " % {"churn_table": churn_table}
    for i in range(len(customers)):
        query_string += " customer_id = '%(id)s' " % {"id": str(customers[i])}
        if i != len(customers) - 1:
            query_string += " OR "
    query_string += ";"
    output = predict(query_string, clf_random_forest)
    return output


# TODO
def predict_all_customers_country(country):
    print "hello"


def save_to_excel(customers, interval):
    now = datetime.datetime.now()
    time_str = now.strftime("%Y%m%dT%H%M%S")
    file_name = interval + "_predictions_" + time_str + ".xlsx"
    predictions_output = os.path.join(home_directory_output, file_name)
    workbook = xlsxwriter.Workbook(predictions_output)
    worksheet = workbook.add_worksheet()
    worksheet.write('A1', 'Customer ID')
    worksheet.write('B1', 'Churn Probability')
    for i in range(len(customers)):
        worksheet.write(i+1, 0, customers[i]['customerId'])
        worksheet.write(i+1, 1, float(customers[i]['churnProbability']))
    workbook.close()


def save_to_excel(classifier_name, customers, interval, time_str):
    file_name = interval + "_" + classifier_name + "_predictions_" + time_str + ".xlsx"
    predictions_output = os.path.join(output_prediction_directory, file_name)
    workbook = xlsxwriter.Workbook(predictions_output)
    worksheet = workbook.add_worksheet()
    worksheet.write('A1', 'Customer ID')
    worksheet.write('B1', 'Churn Probability')
    for i in range(len(customers)):
        worksheet.write(i+1, 0, customers[i]['customerId'])
        worksheet.write(i+1, 1, float(customers[i]['churnProbability']))
    workbook.close()

##########################################################################

# save_to_excel(predict_all_customers())

# output = predict_all_customers()
# print(output)

# output = predict_specific_customers({'x': 20367487, 'y': 21335511})
# print(output)
