import psycopg2 as psycopg2

import myLog


def connect_redshift():
    # Obtaining the connection to RedShift
    connection_string = "dbname='planumdb' port='5439' user='admin' password='E8Ac2euC' " \
                        "host='markets-bi.c0dv9mertkga.eu-west-1.redshift.amazonaws.com'";
    print "Connecting to RedShift"
    conn = psycopg2.connect(connection_string)
    return conn


def query_redshift(query_string):
    conn = connect_redshift()
    cur = conn.cursor()
    myLog.log("Execute: " + query_string)
    cur.execute(query_string)
    rows = cur.fetchall()
    conn.commit()
    conn.close()
    return rows


def get_columns_names(table_name):
    conn = connect_redshift()
    cur = conn.cursor()
    cur.execute("SELECT * FROM %(table_name)s LIMIT 0" % {"table_name": table_name})
    columns_names = [desc[0] for desc in cur.description]
    conn.commit()
    conn.close()
    return columns_names
