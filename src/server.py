import bottle
from bottle import Bottle, get, run, request, response, abort, post, route
import json

from prediction import *


def enable_cors(fn):
    def _enable_cors(*args, **kwargs):
        # set CORS headers
        response.headers['Access-Control-Allow-Origin'] = '*'
        response.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, OPTIONS'
        response.headers[
            'Access-Control-Allow-Headers'] = 'Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token'

        if bottle.request.method != 'OPTIONS':
            # actual request; reply with the actual response
            return fn(*args, **kwargs)

    return _enable_cors


@get('/')
def hello():
    return "Hello World!"


@get('/churn/allUsers')
@enable_cors
def predictAllUsers():
    output = predict_all_customers()
    if output is not None:
        print "Output length: " + str(len(output))
        # Set Json response from function
        response.body = json.dumps(output)
        return response
    else:
        abort(466, "Sorry, unexpected error occurred.")


@route('/churn/user', method=['OPTIONS', 'POST'])
@enable_cors
def predictCustomer():
    if request.method == 'OPTIONS':
        response.body = {}
        return response
    else:
        aList = dict(request.json)
        output = predict_specific_customers(aList['customerList'])

        if output is not None:
            # Set Json response from function
            response.body = json.dumps(output)
            return response
        else:
            abort(466, "Sorry, the customers ids were not found.")


@route('/churn/userList', method=['OPTIONS', 'POST'])
@enable_cors
def predictCustomerList():
    if request.method == 'OPTIONS':
        response.body = {}
        return response
    else:
        aList = dict(request.json)
        output = predict_specific_customers(aList['customersList'])

        if output is not None:
            print "Output length: " + str(len(output))
            # Set Json response from function
            response.body = json.dumps(output)
            return response
        else:
            abort(466, "Sorry, the customers ids were not found.")


@route('/churn/parameters', method=['OPTIONS', 'POST'])
@enable_cors
def getUserPredictionsByParameters():
    if request.method == 'OPTIONS':
        response.body = {}
        return response
    else:
        aList1 = dict(request.json[0])
        if aList1.has_key('baseProbability'):
            aList2 = dict(request.json[1])
            output = predict_all_customers_prob(aList1['baseProbability'], str(aList2['direction']))

        if output is not None:
            print "Output length: " + str(len(output))
            # Set Json response from function
            response.body = json.dumps(output)
            return response
        else:
            abort(466, "Sorry, unexpected error occurred.")


run(host='localhost', port=8080, debug=True)
